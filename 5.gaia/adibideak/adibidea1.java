package adibidea;

class Kontrolatzailea {
	private int kop;
	private int plazak;
	Kontrolatzailea(int p)
	{plazak=p; kop=0;}
	synchronized void sartu() throws InterruptedException { 

		while (!(kop<plazak)) wait();	
		++kop;
		System.out.println("Sartu da kotxe bat");
		System.out.println("\t\t\t\t\t\t"+kop);
		notify();
	}

	synchronized void irten() throws InterruptedException {
		while (!(kop>0)) wait();	
		--kop;
		System.out.println("\t\t\tIrten da kotxe bat");
		System.out.println("\t\t\t\t\t\t"+kop);
		notify();
	}
}

class Sarrerak extends Thread {
	Kontrolatzailea aparkalekua;
	public Sarrerak(Kontrolatzailea k){
		aparkalekua = k;
	}
	public void run() {
		while(true) {
			try {
				aparkalekua.sartu();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				sleep((int)(Math.random()*1500));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}

class Irteerak extends Thread {
	Kontrolatzailea aparkalekua;
	public Irteerak(Kontrolatzailea k){
		aparkalekua = k;
	}
	public void run() {
		while(true) {
			try {
				aparkalekua.irten();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				sleep((int)(Math.random()*1500));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}


class AparkalekuaApp{
	final static int Plazak = 4;
	public static void main (String args[]) {
		Kontrolatzailea k = new Kontrolatzailea (Plazak);
		Sarrerak sar = new Sarrerak(k);
		Irteerak irt = new Irteerak(k);
		sar.start();
		irt.start();
	}
}