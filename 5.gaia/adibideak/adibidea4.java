package semaforoa;

 class Semaforo {
	private int balioa;
	public Semaforo (int hasierakoa)
	{balioa = hasierakoa;}

	synchronized public void gora() {
		++balioa;
		notify();
	}
	synchronized public void behera() throws InterruptedException {

		while (!(balioa>0)) wait();
		--balioa;
	}
}


class MutexLoop extends Thread {
	
	Semaforo mutex;
	String tartea;
	int luze;
	
	
	public MutexLoop (Semaforo sema, int zenbat, String tabul) {
	mutex=sema; luze=zenbat; tartea=tabul;

	}	
	public void run(){
		try {while(true) {
			for (int i=1;i<=6;i++)
				bisualizatu("|");
			mutex.behera(); // eskuratu elkar-bazterketa
			for (int i=1;i<=luze;i++) bisualizatu("*");// Ekintza kritikoa
			mutex.gora();// askatu elkar-bazterketa

		}
		} catch(InterruptedException e){}
	}
	void bisualizatu(String ikurra) {
		try {System.out.println(tartea+ikurra);
		sleep((int)(Math.random()*1000));
		}catch (InterruptedException e) {}
	}

}

class nagusia{
	public static void main (String[] args){
		
		Semaforo s = new Semaforo(1);
		MutexLoop m1 = new MutexLoop(s,8,"\t");
		MutexLoop m2 = new MutexLoop(s,8,"\t\t");
		MutexLoop m3 = new MutexLoop(s,8,"\t\t\t");
		
		m1.start();
		m2.start();
		m3.start();

		
	}
}