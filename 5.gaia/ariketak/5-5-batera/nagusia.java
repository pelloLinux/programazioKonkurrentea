/**
 * 5.5.batera
 * 
 * Hainbat prozesu sinkronizatzen dira denek batera ekintza jakin bat egiteko.
 * 
 * 2016/10/31
 * Pello Arrue Aldalur
 */


// ||NAGUSIA = (p[PR]:P || BATERA).
public class nagusia {


	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int prozesuKop = 4;
		int botea = 1;
		//pantalla sortu
		pantalla p = new pantalla(prozesuKop+botea);
		//batera sortu
		batera b = new batera(prozesuKop,p);

		prozesua[] prozesuak = new prozesua[prozesuKop];
		for(int i=0; i<prozesuKop; i++){
			prozesuak[i] = new prozesua(i+1,b);
		}
		//goibururkoa inprimatu
		p.goiburukoaInprimatu(prozesuKop);
		//ariak exekutatu
		for(int i=0; i<prozesuKop; i++)
			prozesuak[i].start();


	}

}
