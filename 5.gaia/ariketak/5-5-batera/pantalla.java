
class pantalla {

	private int kopPosizioak;

	public pantalla(int kopPosizioak){
		this.kopPosizioak=kopPosizioak;
	}

	public void goiburukoaInprimatu(int kop){


		for(int i=1; i<=kop; i++){
			System.out.print("b["+i+"]");
			System.out.print("\t\t");
		}
		System.out.println();
		System.out.print("============================");
		for(int i=1; i<=kop; i++)
			System.out.print("===================");
		System.out.println();


	}
	public synchronized void inprimatu(String s,int puskaop, int pos, int kop, int kopMax){


		//espazioak inprimatu
		for(int i=0; i<pos; i++)System.out.print("\t\t");
		System.out.print(s+"["+puskaop+"]");//arazoa honek ematen du(bi zifrako zenbakiek)
		for(int i=0; i<this.kopPosizioak-pos-1; i++)System.out.print("\t\t");
		//if(suk)System.out.print("\t");
		kopuruaInprimatu(kop,kopMax);
		notify();
	}
	public synchronized void inprimatu2(String s, int pos, int kop, int kopMax){


		//espazioak inprimatu
		for(int i=0; i<pos; i++)System.out.print("\t\t");
		System.out.print(s);//arazoa honek ematen du(bi zifrako zenbakiek)
		for(int i=0; i<this.kopPosizioak-pos-1; i++)System.out.print("\t\t");
		//if(suk)System.out.print("\t");
		kopuruaInprimatu(kop,kopMax);
		notify();
	}

	private void kopuruaInprimatu(int kop, int kopMax){

		System.out.print("[");
		for(int i=0; i<kop; i++)
			System.out.print("*");
		for(int i=kop; i<kopMax; i++)
			System.out.print(" ");
		System.out.print("]");
		System.out.println();
		
	}

	public int getKopPosizioak() {
		return kopPosizioak;
	}

	public void setKopPosizioak(int kopPosizioak) {
		this.kopPosizioak = kopPosizioak;
	}

}