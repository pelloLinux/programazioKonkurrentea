
public class batera {

	private int kop;
	private int maxKop;
	private pantalla p;

	//when(k<PK)				p[PR].iritsi->	BATERA[k+1]
	public synchronized void iritsi(prozesua p) throws InterruptedException{

		while (!(kop<maxKop))wait(); 
		kop++;
		this.p.inprimatu2("iritsi", p.iz-1, kop, maxKop);
		notifyAll();

	}


	//when(k>=PK && k<2*PK)	p[PR].egin	->	BATERA[k+1]
	public synchronized void egin(prozesua p) throws InterruptedException{

		while (!(kop>=maxKop && kop<2*maxKop))wait(); 
		kop++;
		if(kop%maxKop == 0)
			this.p.inprimatu2("egin", p.iz-1, (maxKop), maxKop);
		else
			this.p.inprimatu2("egin", p.iz-1, kop%(maxKop), maxKop);
		notifyAll();

	}

	//	when(k>=2*PK && k<3*PK)	p[PR].joan	-> 	if(k<(3*PK)-1)then BATERA[k+1]	
	//											else 		   BATERA[0] 
	public synchronized void joan(prozesua p) throws InterruptedException{

		while (!(kop>=2*maxKop && kop<3*maxKop))wait(); 
		kop++;
		//ondo inprimatzeko
		if(kop%maxKop == 0)
			this.p.inprimatu2("joan", p.iz-1, (maxKop), maxKop);
		else
			this.p.inprimatu2("joan", p.iz-1, kop%(maxKop), maxKop);
		
		if(kop>(3*maxKop)-1)
			kop=0;
		
		notifyAll();

	}

	public batera(int maxKop, pantalla p)
	{
		this.maxKop = maxKop;
		this.p = p;
	}


	public int getKop() {
		return kop;
	}

	public void setKop(int kop) {
		this.kop = kop;
	}

}
