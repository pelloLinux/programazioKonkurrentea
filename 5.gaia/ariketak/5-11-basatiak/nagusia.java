/**
 * 5.11.basatiak
 * 
 * 11. Basatien festa eroa:
	Misiolariak iristean, sukaldariak akatzen ditu, zatitu, puskak hozkailuan sartu, eta
	hortik lapikora..., begiratuz beti ea tokia dagoen lapikoan, hozkailuan...
 * 
 * Pello Arrue
 * 
 *2016/10/31
 */

//||JANARIA = ( b[BR]:BASATIA || s:SUKALDARIA || LAPIKOA || HOZKAILUA || PLAIA || t:TXALUPA).

class nagusia{
	public static void main (String args[]) {

		int basatiKop = 3;
		int lapikoa =1 ;
		int sukaldaria = 1;
		int txalupa = 1;
		int hondartza = 1;
		int hozkailua = 1;

		//pantalla sortu
		pantalla p = new pantalla(basatiKop+lapikoa+sukaldaria+txalupa+hondartza+hozkailua);
		//Lapikoa sortu
		lapikoa l = new lapikoa(0,10,true,p,1+basatiKop+1+1);
		//plaia sortu
		plaia pl = new plaia(0, 10, p,1+basatiKop+1);
		//hozkalilua sortu
		hozkailua h = new hozkailua(0, 10, p,1+basatiKop+1+1);
		//pantallari klaseak ezarri
		p.setP(pl);
		p.setL(l);
		p.setH(h);
		//txalupa sortu
		txalupa t = new txalupa(3, 0, pl);
		//Sukaldaria sortu
		sukaldaria s = new sukaldaria("1",2+basatiKop,l,pl,h,p);
		//basatiakSortu
		basatia[] basatiak = new basatia[basatiKop];
		for(int i=0; i<basatiKop; i++){
			basatiak[i] = new basatia(String.valueOf(i+1),l,i+2,-1,p);
		}
		//goibururkoa inprimatu
		p.goiburukoaInprimatu("txalupa", basatiKop, "suk", "hondartza", "hozkailua", "lapikoa");
		//ariak exekutatu
		t.start();
		s.start();
		for(int i=0; i<basatiKop; i++)
			basatiak[i].start();

	}


}
