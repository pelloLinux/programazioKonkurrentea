
public class txalupa extends Thread{

	private int inprPos;
	private int kop;
	private plaia p;

	//eraikitzailea
	public txalupa(int kop, int inpr,  plaia p){
		this.kop = kop;
		this.setInprPos(inpr);
		this.p = p;
	}
	
//	TXALUPA = (plaianBegiratu[p:PR]-> if(TK<=PK-p)then(misiolariakLurreratu[TK]->TXALUPA)
//			  else (plaiaBegiratzeariUtzi -> TXALUPA)
//).

	public void run() {

		try {

			while(true){
				
				//plaianBegiratu[p:PR]
				int plaian = p.plaianBegiratuTxalupatik(p,this);
				//if(TK<=PK-p)then
				if(kop<=p.plaiaBeteKop - plaian)
				{	//misiolariakLurreratu[TK]
					p.misiolariakPlaianLaga(kop, p);
				}
				else//plaiaBegiratzeariUtzi 
					p.plaiaBegiratzeariUtzi();
				sleep(10000);

			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getInprPos() {
		return inprPos;
	}

	public void setInprPos(int inprPos) {
		this.inprPos = inprPos;
	}


}
