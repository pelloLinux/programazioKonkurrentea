
class pantalla {

	private int kopPosizioak;
	private plaia p;
	private hozkailua h;
	private lapikoa l;
	

	public pantalla(int kopPosizioak){
		this.kopPosizioak=kopPosizioak;
	}

	public void goiburukoaInprimatu(String iz1, int kop, String iz2, String iz3, String iz4, String iz5){

		System.out.print(iz1);
		System.out.print("\t\t");
		for(int i=1; i<=kop; i++){
			System.out.print("b["+i+"]");
			System.out.print("\t");
		}

		System.out.print(iz2);		
		System.out.print("\t\t");
		System.out.print(iz3);
		System.out.print("\t");
		System.out.print(iz4);
		System.out.print("\t");
		System.out.println(iz5);
		System.out.print("==============================================");
		for(int i=1; i<=kop; i++)
			System.out.print("===================");
		System.out.println();
		
		
		
		for(int i=0; i<this.kopPosizioak-1; i++)System.out.print("\t");
		kopuruaInprimatu(p.misiolariKop, p.plaiaBeteKop);
		System.out.print("\t");
		kopuruaInprimatu(h.puskaKop, h.hozkailuaBeteKop);
		System.out.print("\t");
		kopuruaInprimatu(l.puskaKop, l.lapikoaBeteKop);
		System.out.println();	


	}
	public synchronized void inprimatu(String s,int puskaop, int pos, int kop, int kopMax){


		//espazioak inprimatu
//		for(int i=0; i<pos; i++)System.out.print("\t");
//		System.out.print(s+"["+puskaop+"]");
//		for(int i=0; i<this.kopPosizioak-pos-1; i++)System.out.print("\t");
//		//if(suk)System.out.print("\t");
//		kopuruaInprimatu(kop, kopMax);
//		notify();
	}
	
	public synchronized void inprimatu2(String s,int pos){


		//espazioak inprimatu
		for(int i=0; i<pos; i++)System.out.print("\t");
		System.out.print(s);
		for(int i=0; i<this.kopPosizioak-pos-1; i++)System.out.print("\t");
		kopuruaInprimatu(p.misiolariKop, p.plaiaBeteKop);
		System.out.print("\t");
		kopuruaInprimatu(h.puskaKop, h.hozkailuaBeteKop);
		System.out.print("\t");
		kopuruaInprimatu(l.puskaKop, l.lapikoaBeteKop);
		System.out.println();	
		notify();
	}
	
	public synchronized void inprimatuBegiratu(String s,int pos){


		//espazioak inprimatu
		//System.out.print("\t");
		for(int i=0; i<pos; i++)System.out.print("\t");
		System.out.print(s);
//		for(int i=0; i<this.kopPosizioak-pos-1; i++)System.out.print("\t");
//		kopuruaInprimatu(p.misiolariKop, p.plaiaBeteKop);
//		System.out.print("\t");
//		kopuruaInprimatu(h.puskaKop, h.hozkailuaBeteKop);
//		System.out.print("\t");
//		kopuruaInprimatu(l.puskaKop, l.lapikoaBeteKop);
		System.out.println();	
		notify();
	}

	private void kopuruaInprimatu(int kop, int kopMax){

		System.out.print("[");
		for(int i=0; i<kop; i++)
			System.out.print("*");
		
		for(int i=kop; i<kopMax; i++)
			System.out.print(" ");
		System.out.print("]");


	}

	public int getKopPosizioak() {
		return kopPosizioak;
	}

	public void setKopPosizioak(int kopPosizioak) {
		this.kopPosizioak = kopPosizioak;
	}

	public plaia getP() {
		return p;
	}

	public void setP(plaia p) {
		this.p = p;
	}

	public hozkailua getH() {
		return h;
	}

	public void setH(hozkailua h) {
		this.h = h;
	}

	public lapikoa getL() {
		return l;
	}

	public void setL(lapikoa l) {
		this.l = l;
	}

}