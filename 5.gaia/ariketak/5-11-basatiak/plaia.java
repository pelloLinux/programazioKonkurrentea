
public class plaia {

	public int misiolariKop;
	public int plaiaBeteKop;
	private pantalla p;
	public int pos;
	private boolean libre;

	
	//eraikitzailea
	public plaia(int kopHasieran, int plaiaBete, pantalla p, int pos){
		this.misiolariKop = kopHasieran;
		this.plaiaBeteKop = plaiaBete;
		this.p = p;
		this.pos = pos;
		this.libre = true;

	}
	
	//when(j==0)	 s.plaianBegiratu[i] 		  	   -> PLAIA[i][1]
	public synchronized int plaianBegiratu(plaia p, sukaldaria s) throws InterruptedException{
		
		while (this.libre == false)wait();

		int itzultzeko = p.misiolariKop;
		this.p.inprimatuBegiratu("begPlaian "+itzultzeko, s.getInprPos());
		this.libre = false;
		notifyAll();
		return itzultzeko;
	}
	
	//when(j==0)	 t.plaianBegiratu[i] 		  	   -> PLAIA[i][1]
	public synchronized int plaianBegiratuTxalupatik(plaia p, txalupa t) throws InterruptedException{
		
		while (this.libre == false)wait();

		int itzultzeko = p.misiolariKop;
		this.p.inprimatuBegiratu("beg "+itzultzeko, t.getInprPos());
		this.libre = false;
		notifyAll();
		return itzultzeko;
	}
	
	//| 			 s.misiolariakEhizatu[k:1..PK]    -> PLAIA[i-k][0]
	public synchronized void misiolariakEhizatu(int zenbat, plaia p){
		
		p.misiolariKop -= zenbat;
		this.libre = true;
		notifyAll();
	}
	
	//| 			 t.misiolariakLurreratu[TK]       -> PLAIA[i+TK][0]
	public synchronized void misiolariakPlaianLaga(int zenbat, plaia p)
	{
		p.misiolariKop += zenbat;
		this.p.inprimatu2("irts "+zenbat, 0);
		this.libre = true;
		notifyAll();
	}
	
//	|when(j==1)  s.plaiaBegiratzeariUtzi		   ->PLAIA[i][0]
//	|when(j==1)  t.plaiaBegiratzeariUtzi		   ->PLAIA[i][0]
	public synchronized void plaiaBegiratzeariUtzi() throws InterruptedException{
		
		this.libre = true;
		notifyAll();
	}

}
