
public class hozkailua {

	public int puskaKop;
	public int hozkailuaBeteKop;
	public int pos;
	private pantalla p;

	//eraikitzailea
	public hozkailua(int kopHasieran, int hozkailuaBete, pantalla p, int pos){
		this.puskaKop = kopHasieran;
		this.hozkailuaBeteKop = hozkailuaBete;
		this.p = p;
		this.pos = pos;

	}

	//s.hozkailuanBegiratu[i] 			-> 	HOZKAILUA[i]
	public synchronized int hozkailuanBegiratu(hozkailua h, sukaldaria s){
		
		int itzultzeko = h.puskaKop;
		this.p.inprimatuBegiratu("begHozkail "+itzultzeko, s.getInprPos());
		notifyAll();
		return itzultzeko;
	}

	//s.hozkailuanSartu[k:ZATIAK..HK]	->	HOZKAILUA[i+k]
	public synchronized void hozkailuanSartu(int zenbat, hozkailua h, sukaldaria s){

		h.puskaKop += zenbat;
		this.p.inprimatu2("sart "+zenbat, s.getInprPos());
		notifyAll();

	}

	//s.hozkailutikAtera[k:ZATIAK..HK]	->	HOZKAILUA[i-k]
	public synchronized void hozkailutikAtera(int zenbat, hozkailua h, sukaldaria s){

		h.puskaKop -= zenbat;
		this.p.inprimatu2("ater "+zenbat, s.getInprPos());
		notifyAll();

	}


}
