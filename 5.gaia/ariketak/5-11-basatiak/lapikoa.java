class lapikoa {

	public int puskaKop;
	public int lapikoaBeteKop;
	private boolean libre;
	private pantalla p;
	public int pos;

	
	//eraikitzailea
	public lapikoa(int kopHasieran, int lapikoaBete, boolean libre, pantalla p, int pos){
		this.puskaKop = kopHasieran;
		this.lapikoaBeteKop = lapikoaBete;
		this.libre = libre;
		this.p = p;
		this.pos = pos;

	}

	//when(i<N)	s.bota[b:1..N-i] -> LAPIKOA[i+b][0]
	public synchronized void lapikoaBete(sukaldaria s,int kop, lapikoa l) throws InterruptedException
	{
		while (!(this.puskaKop<this.lapikoaBeteKop))wait(); 
		this.puskaKop+=kop;
		this.p.inprimatu2("bota "+kop,s.getInprPos());
		this.libre=true;
		notifyAll();
	}
	//when(i>0)	b[BR].hartu[a:1..i] ->	LAPIKOA[i-a][0]
	public synchronized void puskaHartu(basatia b, int kop, lapikoa l) throws InterruptedException{

		while (!(this.puskaKop>0))wait(); 
		this.puskaKop-=kop;
		this.p.inprimatu2("hartu "+kop,b.getInprPos());
		this.libre=true;
		notifyAll();
	}
	
//	|when(j==0)			b[BR].lapikoanBegiratu[i] 			-> 	LAPIKOA[i][1]
	public synchronized int begiratuSukaldariak(sukaldaria s) throws InterruptedException{
		
		while (this.libre == false)wait();

		this.libre=false;	
		int itzuli =  this.getPuskaKop();
		p.inprimatuBegiratu("begLapikoa "+itzuli, s.getInprPos());
		notifyAll();
		return itzuli;
	}
	
//	|when(j==0)			b[BR].lapikoanBegiratu[i] 			-> 	LAPIKOA[i][1]
	public synchronized int begiratuBasatia(basatia b) throws InterruptedException{
		
		while (this.libre == false)wait();

		this.libre=false;	
		int itzuli =  this.getPuskaKop();
		p.inprimatuBegiratu("beg "+itzuli, b.getInprPos());
		notifyAll();
		return itzuli;
	}
	
	//|when(j==1)			b[BR].lapikoaBegiratzeariUtzi		->  LAPIKOA[i][0]
	//|when(j==1)			s.lapikoaBegiratzeariUtzi			->  LAPIKOA[i][0]
	public synchronized void lapikoanBegiratzeariUtzi() throws InterruptedException{
		
		this.libre = true;
		notifyAll();
	}
	
	public int getPuskaKop() {
		return puskaKop;

	}
	public void setPuskaKop(int puskaKop) {
		this.puskaKop = puskaKop;
	}

	public int getLapikoaBeteKop() {
		return lapikoaBeteKop;
	}

	public void setLapikoaBeteKop(int lapikoaBeteKop) {
		this.lapikoaBeteKop = lapikoaBeteKop;
	}

	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}
}
