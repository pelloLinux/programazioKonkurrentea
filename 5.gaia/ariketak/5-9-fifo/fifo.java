

public class fifo {
	
	private int sartuInd;
	private int ateraInd;
	private int kop;
	private int max;
	private int[] fifo;
	private pantalla pantalla;
	
	//eraikitzailea
	fifo(int k, pantalla p){
		sartuInd = 0;
		ateraInd = 0;
		kop = 0;
		max = k;
		pantalla = p;
		fifo = new int[k];
	}
	
	//when(k<FK)sartu_return[s]	-> FIFO [(s+1)%FK][a][k+1]
	public synchronized int sartu(prozesu p) throws InterruptedException{
		
		while (!(kop<max))wait();
		int lag = sartuInd;
		fifo[sartuInd]=1;
		sartuInd = (sartuInd+1)%max;
		kop++;
		notifyAll();
		pantalla.inprimatu("s", lag, p.getIzena(), kop, max, fifo);
		return lag;
		
	}
	//			atera[a]	-> FIFO [s][(a+1)%FK][k-1]
	public synchronized void atera(prozesu p,int prozInd) throws InterruptedException{
		
		while (!(prozInd == ateraInd))wait();
		fifo[ateraInd]=0;
		pantalla.inprimatu("a", ateraInd, p.getIzena(), kop, max,fifo);
		ateraInd = (ateraInd+1)%max;
		kop--;
		notifyAll();
	}

}
