


class pantalla {

	private int kopPosizioak;

	public pantalla(int kopPosizioak){
		this.kopPosizioak=kopPosizioak;
	}

	public void goiburukoaInprimatu(int kop, String iz2){


		for(int i=1; i<=kop; i++){
			System.out.print("p["+i+"]");
			System.out.print("\t");
		}
		System.out.print("\t");
		System.out.println(iz2);
		System.out.print("============================");
		for(int i=1; i<=kop; i++)
			System.out.print("========");
		System.out.println();


	}
	public synchronized void inprimatu(String s,int ind, int pos, int kop, int kopMax, int[] b){


		//espazioak inprimatu
		for(int i=0; i<pos; i++)System.out.print("\t");
		System.out.print(s+"["+ind+"]");//arazoa honek ematen du(bi zifrako zenbakiek)
		for(int i=0; i<this.kopPosizioak-pos-1; i++)System.out.print("\t");
		System.out.print("\t");
		kopuruaInprimatu(b);
		notify();
	}

	private void kopuruaInprimatu(int[] b){

		System.out.print("|");
		for(int i=0; i<b.length; i++)
			if(b[i]==1)
				System.out.print("*|");
			else if(b[i]==0)
				System.out.print(" |");
		System.out.println();

	}

	public int getKopPosizioak() {
		return kopPosizioak;
	}

	public void setKopPosizioak(int kopPosizioak) {
		this.kopPosizioak = kopPosizioak;
	}

}