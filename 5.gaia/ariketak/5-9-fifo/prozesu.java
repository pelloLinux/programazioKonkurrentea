
public class prozesu extends Thread {

	private int izena;
	//	private int gordetakoInd;
	private fifo fifo;

	//eraikitzailea
	prozesu(int iz, fifo f){
		setIzena(iz);
		//gordetakoInd = -1;
		fifo = f;
	}

	//P = (sartu_return[i:FR] -> atera[i]-> P).
	public void run() {

		try {

			while(true)
			{
				//sartu_return[i:FR]
				int gordetakoInd = fifo.sartu(this);
				sleep((int)(Math.random()*2000+500));
				//atera[i]
				fifo.atera(this,gordetakoInd);
				sleep((int)(Math.random()*2000+500));
			}


		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getIzena() {
		return izena;
	}

	public void setIzena(int izena) {
		this.izena = izena;
	}

}
