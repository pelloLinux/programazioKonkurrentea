

public class nagusia {

	/**
	 * 5.9.fifo
	 * 
	 * 9. FIFO ilara batean prozesuak sartu eta ateratzen dira.
	 * 
	 * Pello Arrue
	 * 
	 *2016/09/30
	 */
	
	//||NAGUSIA = (p[PR]:P || p[PR]::FIFO).
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int prozesuKop = 10;
		int fifoMax = 8;
		//pantalla sortu
		pantalla p = new pantalla(prozesuKop);
		//fifoa sortu
		fifo fifo = new fifo(fifoMax, p);
		//prozesuak sortu
		prozesu[] prozesuak = new prozesu[prozesuKop];
		for(int i = 0; i<prozesuKop; i++)
			prozesuak[i] = new prozesu(i, fifo);
		//goiburukoa inpriomatu
		p.goiburukoaInprimatu(prozesuKop, "fifo");
		//ariak hasieratu
		for(int i = 0; i<prozesuKop; i++)
			prozesuak[i].start();
		

	}

}
