/**
 * 8. Array bateko zenbakien artean maximoa aukeratu, agendaren eredua erabiliz.
Ondo pentsatu noiz bukatzen den prozesaketa.
 * 
 * Pello Arrue
 * 
 *2016/10/31
 */

//||MAX = (p[PR]:PROZ || AGENDA).

class nagusia{
	public static void main (String args[]) {

		int prozesuKop = 3;


		//pantalla sortu
		pantalla p = new pantalla(prozesuKop);
		//agenda sortu
		agenda a = new agenda(10, p);
		//prozesuakSortu
		prozesu[] prozesuak = new prozesu[prozesuKop];
		for(int i=0; i<prozesuKop; i++){
			prozesuak[i] = new prozesu(i,a,p);
		}
		//goibururkoa inprimatu
		p.goiburukoaInprimatu(prozesuKop);
		p.inprimatu3(a.array);
		//ariak exekutatu

		for(int i=0; i<prozesuKop; i++)
			prozesuak[i].start();

	}


}
