
public class agenda {
	
	int lanean;
	int i;
	int[] array;
	pantalla p;
	
	agenda(int N, pantalla p){
		this.i=N;
		this.lanean =0;
		this.array = new int[N+1];
		//ausaz bete;
		array[0]=-1;
		for(int j=1; j<N+1; j++){
			array[j]=(int)(Math.random()*100);
		}
		this.p = p;
	}
	
	//when(i>1)	p[PR].hartu-> [i][i-1] -> AGENDA[i-2][lan+1]
	public synchronized int[] hartu(prozesu p) throws InterruptedException
	{
		while (!(i>1))wait(); 
		int[] r = {array[i-1],array[i]};
		array[i]=-1;
		array[i-1]=-1;
		this.p.inprimatu("hartu["+r[0]+"]["+r[1]+"]", p.getIzena(),array);
		i-=2;
		lanean++;
		notifyAll();
		return r;
	}
	
	
//	p[PR].sartu-> [i+1]	   -> if(i==0 && lan==1) then	(emaitza -> STOP )
//							  else   AGENDA[i+1][lan-1]
	public synchronized void sartu(prozesu p, int max)
	{
		
		array[i+1]=max;
		this.p.inprimatu("sartu["+max+"]", p.getIzena(),array);
		if(i==0 && lanean==1){
			System.out.println("................. BUKATUTA ......................");
			System.exit(0);
		}
		else
		{
			i++;
			lanean--;
		}
		notifyAll();
	}

}
