
public class prozesu extends Thread {

	private int izena;
	private agenda a;
	private pantalla p;


	//eraikitzailea
	prozesu(int iz, agenda a, pantalla p){
		this.izena = iz;
		this.a = a;
		this.p = p;
	}

	//PROZ = (hartu -> konparatu -> sartu -> PROZ).
	public void run() {

		try {
			while(true)
			{
				int[] zenbs = a.hartu(this);
				sleep((int)(Math.random()*2000+500));
				p.inprimatu2("konparatu", this.izena);
				if(zenbs[0]>=zenbs[1])
					a.sartu(this,zenbs[0]);
				else 
					a.sartu(this, zenbs[1]);
				
				sleep((int)(Math.random()*2000+500));
			}


		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getIzena() {
		return izena;
	}

	public void setIzena(int izena) {
		this.izena = izena;
	}

}
