class lapikoa {

	private int puskaKop;
	private int lapikoaBeteKop;
	private boolean libre;
	private pantalla p;
	
	//eraikitzailea
	public lapikoa(int kopHasieran, int lapikoaBete, boolean libre, pantalla p){
		this.puskaKop = kopHasieran;
		this.lapikoaBeteKop = lapikoaBete;
		this.libre = libre;
		this.p = p;

	}

	//when(i<N)	s.bota[b:1..N-i] -> LAPIKOA[i+b][0]
	public synchronized void lapikoaBete(sukaldaria s,int kop, lapikoa l) throws InterruptedException
	{
		while (!(this.puskaKop<this.lapikoaBeteKop))wait(); 
		this.puskaKop+=kop;
		this.p.inprimatu(" bota",kop,s.getInprPos(),l.getPuskaKop(),l.getLapikoaBeteKop());
		this.libre=true;
		notifyAll();
	}
	//when(i>0)	b[BR].hartu[a:1..i] ->	LAPIKOA[i-a][0]
	public synchronized void puskaHartu(basatia b, int kop, lapikoa l) throws InterruptedException{

		while (!(this.puskaKop>0))wait(); 
		this.puskaKop-=kop;
		this.p.inprimatu("hartu",kop,b.getInprPos(), l.getPuskaKop(),l.getLapikoaBeteKop());
		this.libre=true;
		notifyAll();
	}
	
//	when(j==0) s.begiratu[i] -> if(i!=N) then LAPIKOA[i][1]
//								else 	 	  LAPIKOA[i][0]
	public synchronized int begiratuSukaldariak() throws InterruptedException{
		
		while (!(this.libre))wait();
		if(this.getPuskaKop()==this.lapikoaBeteKop)
			this.libre=true;
		else 
			this.libre=false;
		
		int itzuli =  this.getPuskaKop();
		notifyAll();
		return itzuli;
	}
	
//	when(j==0) b[BR].begiratu[i] -> if(i!=N) then LAPIKOA[i][1]
//	else 	 	  								  LAPIKOA[i][0]
	public synchronized int begiratuBasatia() throws InterruptedException{
		
		while (!(this.libre))wait();
		if(this.getPuskaKop()==0)
			this.libre=true;
		else 
			this.libre=false;
		
		int itzuli =  this.getPuskaKop();
		notifyAll();
		return itzuli;
	}
	
	public int getPuskaKop() {
		return puskaKop;

	}
	public void setPuskaKop(int puskaKop) {
		this.puskaKop = puskaKop;
	}

	public int getLapikoaBeteKop() {
		return lapikoaBeteKop;
	}

	public void setLapikoaBeteKop(int lapikoaBeteKop) {
		this.lapikoaBeteKop = lapikoaBeteKop;
	}

	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}
}
