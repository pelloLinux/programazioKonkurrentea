/**
 * 5.3.basatiak
 * 
 * Basatien festa, baina orain sukaldariak hainbat puska bota eta
basatiek hainbat puska hartu eta, bota edo hartu aurretik, lapikoan zenbat dagoen begiratzen dute
 * 
 * Pello Arrue
 * 
 *2016/09/30
 */
//||JANARIA = ( b[BR]:BASATIA || s:SUKALDARIA || LAPIKOA ).

class nagusia{
	public static void main (String args[]) {

		int basatiKop = 3;
		int lapikoa =1 ;
		int sukaldaria = 1;

		//pantalla sortu
		pantalla p = new pantalla(basatiKop+lapikoa+sukaldaria);
		//Lapikoa sortu
		lapikoa l = new lapikoa(0,9,true,p);
		//Sukaldaria sortu
		sukaldaria s = new sukaldaria("1",0,3,l,p);
		//basatiakSortu
		basatia[] basatiak = new basatia[basatiKop];
		for(int i=0; i<basatiKop; i++){
			basatiak[i] = new basatia(String.valueOf(i+1),l,i+1,-1,p);
		}
		//goibururkoa inprimatu
		p.goiburukoaInprimatu("suk", basatiKop, "lapikoa");
		//ariak exekutatu
		s.start();
		for(int i=0; i<basatiKop; i++)
			basatiak[i].start();

	}


}
