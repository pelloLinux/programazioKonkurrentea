class sukaldaria extends Thread {

	private String izena;
	private int zenbatBota;
	private lapikoa l;
	private int inprPos;
	private pantalla p;

//	SUKALDARIA = ( begiratu[k:KR] ->  if (k<N) then   (random[r:1..N-k] -> bota[r] -> SUKALDARIA )
//			  						  else								   			  SUKALDARIA
//					
//				).
	public void run() {

		try {

			while(true){

				int puskKop = 0;
				//begiratu[k:KR]
				puskKop = l.begiratuSukaldariak();
				this.p.inprimatu("begiratu",puskKop,this.getInprPos(), l.getPuskaKop(),l.getLapikoaBeteKop());
				sleep((int)(Math.random()*1000));
				//if (k<N) then
				if(puskKop<l.getLapikoaBeteKop())
				{
					// (random[r:1..N-k]
					int zenbat = (int)(Math.random()*(l.getLapikoaBeteKop()-puskKop)+1);
					this.p.inprimatu("erabaki",zenbat,this.getInprPos(), l.getPuskaKop(),l.getLapikoaBeteKop());
					// bota[r]
					l.lapikoaBete(this, zenbat, l);
				}
				sleep((int)(Math.random()*1000));

			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//eraikitzailea
	public sukaldaria(String iz, int pos, int bota, lapikoa l, pantalla p){
		this.izena = iz;
		this.inprPos = pos;
		this.l = l;
		this.zenbatBota = bota;
		this.p = p;
	}


	public String getIzena() {
		return izena;
	}


	public void setIzena(String izena) {
		this.izena = izena;
	}


	public lapikoa getL() {
		return l;
	}


	public void setL(lapikoa l) {
		this.l = l;
	}
	public int getInprPos() {
		return inprPos;
	}
	public void setInprPos(int inprPos) {
		this.inprPos = inprPos;
	}
	public int getZenbatBota() {
		return zenbatBota;
	}
	public void setZenbatBota(int zenbatBota) {
		this.zenbatBota = zenbatBota;
	}
}