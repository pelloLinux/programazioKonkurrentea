class basatia extends Thread {

	private String izena;
	private lapikoa l;
	private int inprPos;
	private int zenbatHartu;
	private pantalla p;
	

	//		BASATIA = ( begiratu[k:KR] ->  if (k>0) then   (random[r:1..k] -> hartu[r] -> BASATIA )
	//				   					   else											  BASATIA						
	//				   ).
	public void run() {

		try {
			
			while(true){

				int puskKop = 0;
				//begiratu[k:KR]
				puskKop = l.begiratuBasatia();
				this.p.inprimatu("begiratu",puskKop,this.getInprPos(), l.getPuskaKop(),l.getLapikoaBeteKop());
				sleep((int)(Math.random()*1000));
				//if (k>0) then
				if(puskKop>0)
				{
					//random[r:1..k]
					int zenbat = (int)(Math.random()*puskKop+1);
					this.p.inprimatu("erabaki",zenbat,this.getInprPos(), l.getPuskaKop(),l.getLapikoaBeteKop());
					//hartu[r]
					l.puskaHartu(this, zenbat, l);
				}
				sleep((int)(Math.random()*1000));

			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public basatia(String iz, lapikoa l, int pos, int zenbat, pantalla p){
		this.izena = iz;
		this.l = l;
		this.inprPos = pos;
		this.zenbatHartu = zenbat;
		this.p = p;
	}

	public String getIzena() {
		return izena;
	}

	public void setIzena(String izena) {
		this.izena = izena;
	}

	public lapikoa getL() {
		return l;
	}

	public void setL(lapikoa l) {
		this.l = l;
	}

	public int getInprPos() {
		return inprPos;
	}

	public void setInprPos(int inprPos) {
		this.inprPos = inprPos;
	}

	public int getZenbatHartu() {
		return zenbatHartu;
	}

	public void setZenbatHartu(int zenbatHartu) {
		this.zenbatHartu = zenbatHartu;
	}

}
