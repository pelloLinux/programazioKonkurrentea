class lapikoa {

	private int puskaKop;
	private int lapikoaBeteKop;
	private pantalla p;


	//when(i==0)	s.lapikoaBete		->	KONTR[N]
	public synchronized void lapikoaBete(sukaldaria s, lapikoa l) throws InterruptedException
	{
		while (!(this.puskaKop==0))wait(); 
		this.puskaKop+=this.lapikoaBeteKop;
		this.p.inprimatu("bota",s.getInprPos(),l.getPuskaKop(),l.lapikoaBeteKop);
		notify();
	}
	//when(i>0)		b[BR].puskaHartu	->	KONTR[i-1]
	public synchronized void puskaHartu(basatia b, lapikoa l) throws InterruptedException{

		while (!(this.puskaKop>0))wait(); 
		this.puskaKop--;
		this.p.inprimatu("hartu",b.getInprPos(), l.getPuskaKop(),l.lapikoaBeteKop);
		notify();
	}
	
	public lapikoa(int kopHasieran, int lapikoaBete, pantalla p){
		this.puskaKop = kopHasieran;
		this.lapikoaBeteKop = lapikoaBete;
		this.p = p;

	}
	public int getPuskaKop() {
		return puskaKop;

	}
	public void setPuskaKop(int puskaKop) {
		this.puskaKop = puskaKop;
	}

	public int getLapikoaBeteKop() {
		return lapikoaBeteKop;
	}

	public void setLapikoaBeteKop(int lapikoaBeteKop) {
		this.lapikoaBeteKop = lapikoaBeteKop;
	}
}
