class basatia extends Thread {

	private String izena;
	private lapikoa l;
	private int inprPos;
	private pantalla p;

	//BASATI = (puskaHartu->jan->tripazGora->BASATI).
	public void run() {

		while(true){

			try {
				
				l.puskaHartu(this,this.l);
				sleep((int)(Math.random()*2000+1000));
				this.p.inprimatu("jan",this.getInprPos(),l.getPuskaKop(),l.getLapikoaBeteKop());
				sleep((int)(Math.random()*2000+1000));
				this.p.inprimatu("tripaz",this.getInprPos(),l.getPuskaKop(),l.getLapikoaBeteKop());
				sleep((int)(Math.random()*2000+1000));
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public basatia(String iz, lapikoa l, int pos, pantalla p){
		this.izena = iz;
		this.l = l;
		this.inprPos = pos;
		this.p = p;
	}

	public String getIzena() {
		return izena;
	}

	public void setIzena(String izena) {
		this.izena = izena;
	}

	public lapikoa getL() {
		return l;
	}

	public void setL(lapikoa l) {
		this.l = l;
	}

	public int getInprPos() {
		return inprPos;
	}

	public void setInprPos(int inprPos) {
		this.inprPos = inprPos;
	}

}
