class Buffer{

	int out;
	int in;
	int tam;
	int kont;
	char[] buf;
	pantalla p;
	
	Buffer(int tam, pantalla p){
		
		this.out=0;
		this.in=0;
		this.tam=tam;
		this.kont=0;
		this.buf = new char[tam];
		this.p = p;
	}


	//when (k<N) 	put->[i]->COUNT[(i+1)%N][j]		 [k+1]
	public synchronized void put(char c) throws InterruptedException {

		while (!(kont<tam)) wait();
		buf[in] = c; 
		++kont;
		in=(in+1)%tam;
		p.erakutsi(tam,buf);
		notify();
	}
	//when (k>0) 	get->[j]->COUNT[i]		[(j+1)%N][k-1]
	public synchronized char get() throws InterruptedException {

		while (!(kont>0)) wait();
		char c = (char) buf[out];
		buf[out]=' '; 
		--kont;
		out=(out+1)%tam;
		p.erakutsi(tam,buf);
		notify();
		return (c);
	}

}