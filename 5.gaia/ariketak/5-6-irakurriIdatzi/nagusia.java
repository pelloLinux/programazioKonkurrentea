
public class nagusia {
	

/**
 * 6. Egokitu buffer mugatuaren FSP eredua,
      put eta get egitean, jarri eta hartu behar den posizioa adierazteko
 * 
 * Pello Arrue
 * 
 *2016/10/31
 */
	
//  ||BUFFERMUGATUA = (IDAZLEA||BUFFER||IRAKURLEA).
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		pantalla p = new pantalla();
		Buffer buffer = new Buffer(5,p);
		Idazlea idazle = new Idazlea(buffer,p);
		Irakurlea irakurle = new Irakurlea(buffer,p);
		
		p.goiburua();
		
		idazle.start();
		irakurle.start();
	}

}
