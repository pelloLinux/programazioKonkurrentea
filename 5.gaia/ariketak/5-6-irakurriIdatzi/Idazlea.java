class Idazlea extends Thread {
	
	Buffer buf;
	String alphabet= "abcdefghijklmnopqrstuvwxyz";
	pantalla p;
	//eraikitzailea
	Idazlea(Buffer b, pantalla p) {buf = b; this.p=p;}

//IDAZLEA = (put->IDAZLEA).
	public void run() {

		try {
			int ai = 0;
			while(true) {
				if (Math.random()<0.3) sleep(1000);
				
				p.idatzi(alphabet.charAt(ai));
				buf.put(alphabet.charAt(ai));
				ai=(ai+1)%alphabet.length();
			}
		} catch (InterruptedException e){}
	}
}