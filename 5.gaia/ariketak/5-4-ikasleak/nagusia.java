/**
 * 5.4.iaksleak
 * 
 * Ikasle jator batzuen pisuan gastuetarako bote bat dute. 
	Norberak   ahal   duen   heinean   botean   dirua   sartzen   du,   eta   behar   duen
	neurrian hartu.
 * 
 * 2016/10/11
 * Pello Arrue Aldalur
 */

//||JANARIA = ( b[IKAS]:IKASLEA || BOTEA ).

public class nagusia {


	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int ikasleKop = 4;
		int botea = 1;
		//pantalla sortu
		pantalla p = new pantalla(ikasleKop+botea);
		//Lapikoa sortu
		botea b = new botea(0,20,true,p);
		//ikasleakSortu
		ikaslea[] basatiak = new ikaslea[ikasleKop];
		for(int i=0; i<ikasleKop; i++){
			basatiak[i] = new ikaslea(String.valueOf(i+1),b,i,-1,p);
		}
		//goibururkoa inprimatu
		p.goiburukoaInprimatu(ikasleKop, "botea");
		//ariak exekutatu
		for(int i=0; i<ikasleKop; i++)
			basatiak[i].start();


	}

}
