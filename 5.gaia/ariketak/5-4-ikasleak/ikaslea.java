
class ikaslea extends Thread {

	private String izena;
	private botea b;
	private int inprPos;
	private int zenbatHartu;
	private pantalla pantalla;


	//	                             //hartu
	//IKASLEA = ( erabaki[e:BOOL] -> if (e==0) then (begiratuHartu[i:BOT] -> if(i>0) then (random[r:1..i] -> hartu[r] -> IKASLEA)
	//											   else IKASLEA)
	//	                             //bota
	//	                             else  		  (begiratuBota[i:BOT] -> if(i<N) then (random[r:1..N-i] -> bota[r] -> IKASLEA)															
	//								 			  else IKASLEA)
	//				
	//).

	public void run() {

		try {

			while(true){

				int puskKop = 0;							
				//erabaki[e:BOOL] -> if (e==0) //hartu
				if(Math.random()<0.5){
					this.pantalla.inprimatu2("erabaki.hartu",this.getInprPos(),b.getPuskaKop(),b.getLapikoaBeteKop());
					//begiratuHartu[i:BOT]
					puskKop = b.begiratuHartzeko();
					this.pantalla.inprimatu("begira",puskKop,this.getInprPos(),b.getPuskaKop(),b.getLapikoaBeteKop());
					sleep((int)(Math.random()*1000+1));
					//if(i>0) then
					if(puskKop>0)
					{
						//random[r:1..i]
						int zenbat = (int)(Math.random()*puskKop+1);
						// hartu[r]
						b.diruaHartu(this, zenbat, b);
					}
					sleep((int)(Math.random()*1000+1));
				}
				//bota
				else
				{
					this.pantalla.inprimatu2("erabaki.bota",this.getInprPos(),b.getPuskaKop(),b.getLapikoaBeteKop());
					//begiratuBota[i:BOT]
					puskKop = b.begiratuBotatzeko();
					this.pantalla.inprimatu("begira",puskKop,this.getInprPos(),b.getPuskaKop(),b.getLapikoaBeteKop());
					sleep((int)(Math.random()*1000+1));
					//if(i<N) then
					if(puskKop<b.getLapikoaBeteKop())
					{
						//random[r:1..N-i]
						int zenbat = (int)(Math.random()*(b.getLapikoaBeteKop()-puskKop)+1);
						//bota[r]
						b.diruaBota(this, zenbat, b);
					}
					sleep((int)(Math.random()*1000+1));
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ikaslea(String iz, botea b, int pos, int zenbat, pantalla p){
		this.izena = iz;
		this.b = b;
		this.inprPos = pos;
		this.zenbatHartu = zenbat;
		this.pantalla = p;
	}

	public String getIzena() {
		return izena;
	}

	public void setIzena(String izena) {
		this.izena = izena;
	}


	public int getInprPos() {
		return inprPos;
	}

	public void setInprPos(int inprPos) {
		this.inprPos = inprPos;
	}

	public int getZenbatHartu() {
		return zenbatHartu;
	}

	public void setZenbatHartu(int zenbatHartu) {
		this.zenbatHartu = zenbatHartu;
	}

}