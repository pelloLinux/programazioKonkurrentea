class botea {

	private int puskaKop;
	private int lapikoaBeteKop;
	private boolean libre;
	private pantalla p;
	

	public botea(int kopHasieran, int lapikoaBete, boolean libre, pantalla p){
		this.puskaKop = kopHasieran;
		this.lapikoaBeteKop = lapikoaBete;
		this.libre = libre;
		this.p = p;

	}

	//when(i<N)			b[IKAS].bota[b:1..N-i]		-> 	BOTEA[i+b][0]
	public synchronized void diruaBota(ikaslea s,int kop, botea b) throws InterruptedException
	{
		while (!(this.puskaKop<this.lapikoaBeteKop))wait(); 
		this.puskaKop+=kop;
		this.p.inprimatu(" bota",kop,s.getInprPos(),b.getPuskaKop(),b.getLapikoaBeteKop());
		this.libre=true;
		notifyAll();
	}

	//when(i>0)			b[IKAS].hartu[a:1..i] 		-> 	BOTEA[i-a][0]	
	public synchronized void diruaHartu(ikaslea b, int kop, botea l) throws InterruptedException{

		while (!(this.puskaKop>0))wait(); 
		this.puskaKop-=kop;
		this.p.inprimatu("hartu",kop,b.getInprPos(), l.getPuskaKop(),l.lapikoaBeteKop);
		this.libre=true;
		notifyAll();
	}
	
//	when(j==0)			b[IKAS].begiratuHartu[i] 	-> 	if(i!=0)then 	BOTEA[i][1]
//														else 			BOTEA[i][0]
	public synchronized int begiratuHartzeko() throws InterruptedException{
		
		while (!(this.libre))wait();
		if(this.getPuskaKop()!=0)
			this.libre=false;
		else 
			this.libre=true;
		
		int itzuli =  this.getPuskaKop();
		notifyAll();
		return itzuli;
	}
	
//	when(j==0)			b[IKAS].begiratuBota[i] 	-> 	if(i!=N)then 	BOTEA[i][1]
//														else 			BOTEA[i][0]	
	public synchronized int begiratuBotatzeko() throws InterruptedException{
		
		while (!(this.libre))wait();
		if(this.getPuskaKop()!=this.getLapikoaBeteKop())
			this.libre=false;
		else 
			this.libre=true;
		
		int itzuli =  this.getPuskaKop();
		notifyAll();
		return itzuli;
	}
	
	public int getPuskaKop() {
		return puskaKop;

	}
	public void setPuskaKop(int puskaKop) {
		this.puskaKop = puskaKop;
	}

	public int getLapikoaBeteKop() {
		return lapikoaBeteKop;
	}

	public void setLapikoaBeteKop(int lapikoaBeteKop) {
		this.lapikoaBeteKop = lapikoaBeteKop;
	}

	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}
}