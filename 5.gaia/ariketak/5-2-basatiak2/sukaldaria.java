class sukaldaria extends Thread {

	private String izena;
	private int zenbatBota;
	private lapikoa l;
	private int inprPos;

	public sukaldaria(String iz, int pos, int bota, lapikoa l){
		this.izena = iz;
		this.inprPos = pos;
		this.l = l;
		this.zenbatBota = bota;
	}
	
	//SUKALDARIA = (lapikoaBete->SUKALDARIA).
	public void run() {

		while(true){
			try
			{
				l.lapikoaBete(this,this.l);
				sleep((int)(Math.random()*1000+1000));
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	public String getIzena() {
		return izena;
	}


	public void setIzena(String izena) {
		this.izena = izena;
	}


	public lapikoa getL() {
		return l;
	}


	public void setL(lapikoa l) {
		this.l = l;
	}
	public int getInprPos() {
		return inprPos;
	}
	public void setInprPos(int inprPos) {
		this.inprPos = inprPos;
	}
	public int getZenbatBota() {
		return zenbatBota;
	}
	public void setZenbatBota(int zenbatBota) {
		this.zenbatBota = zenbatBota;
	}
}