
class pantalla {

	private int kopPosizioak;

	public pantalla(int kopPosizioak){
		this.kopPosizioak=kopPosizioak;
	}

	public void goiburukoaInprimatu(String iz1, int kop, String iz2){

		System.out.print(iz1);
		System.out.print("\t\t");
		for(int i=1; i<=kop; i++){
			System.out.print("b["+i+"]");
			System.out.print("\t\t");
		}
		System.out.println(iz2);
		System.out.print("============================");
		for(int i=1; i<=kop; i++)
			System.out.print("================");
		System.out.println();


	}
	public synchronized void inprimatu(String s, int pos, int kop, int max){


		//espazioak inprimatu
		for(int i=0; i<pos; i++)System.out.print("\t\t");
		System.out.print(s);
		for(int i=0; i<this.kopPosizioak-pos-1; i++)System.out.print("\t\t");
		kopuruaInprimatu(kop,max);
		notify();
	}

	private void kopuruaInprimatu(int kop, int max){

		System.out.print("[");
		for(int i=0; i<kop; i++)
			System.out.print("*");
		for(int i=kop; i<max; i++)
			System.out.print(" ");
		
		System.out.print("]");
		System.out.println();

	}

	public int getKopPosizioak() {
		return kopPosizioak;
	}

	public void setKopPosizioak(int kopPosizioak) {
		this.kopPosizioak = kopPosizioak;
	}

}