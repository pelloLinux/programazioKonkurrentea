
public class Pantalla {

	int kotxeKop;
	int oinezKop;
	Pantalla(int o, int k){kotxeKop=k;oinezKop=o;}

	public synchronized void goiburukoaInprimatu(int max){

		for(int i = 0; i<oinezKop; i++)
			System.out.print("o"+i+"\t");

		System.out.print("OZain\t\t");
		System.out.print("Zebra\t\t");
		System.out.print("KZain\t\t");
		
		System.out.print("Atasko\t");	
		System.out.print("Seg\t");

		for(int i = 0; i<kotxeKop; i++)
			System.out.print("k"+i+"\t");

				
		System.out.println();

		for(int i = 0; i<oinezKop+kotxeKop; i++)
			System.out.print("=============");

		System.out.println("========================");

	}

	public synchronized void inprimatuKotx(int pos, String s, Zebrabidea z, int seg, int maxS){
		
		
		for(int i = 0; i<pos+14; i++)System.out.print("\t");
		System.out.print(s);
		System.out.println();
		
		for(int i = 0; i<oinezKop; i++)System.out.print("\t");
		
		//erdikoak
		kopuruaInprimatuO(z.zainO,oinezKop);
		System.out.print("\t");
		if(z.zebraK>0)
			kopuruaInprimatuK(z.zebraK,oinezKop);
		else if(z.zebraO>=0)
			kopuruaInprimatuO(z.zebraO,oinezKop);
		System.out.print("\t");
		kopuruaInprimatuK(z.zainK,kotxeKop);
		//////////////////////
		System.out.print("\t");
	
		
		
		if(z.zainK>3)
			System.out.print("*");
		else System.out.print(z.zainK);
		System.out.print("\t");
		if(seg<maxS-1)
			System.out.print(seg);
		else System.out.print("*");
	



		for(int i = pos; i<kotxeKop; i++)System.out.print("\t");


		System.out.println();

	}

	public synchronized void inprimatuOinez(int pos, String s, Zebrabidea z, int seg, int maxS){

		for(int i = 0; i<pos; i++)System.out.print("\t");
		System.out.print(s);

		System.out.println();

		for(int i = 0; i<oinezKop; i++)System.out.print("\t");

		kopuruaInprimatuO(z.zainO,oinezKop);
		System.out.print("\t");
		if(z.zebraK>0)
			kopuruaInprimatuK(z.zebraK,oinezKop);
		else if(z.zebraO>=0)
			kopuruaInprimatuO(z.zebraO,oinezKop);

		System.out.print("\t");
		kopuruaInprimatuK(z.zainK,kotxeKop);
		
		System.out.print("\t");
		
		if(z.zainK>3)
			System.out.print("*");
		else System.out.print(z.zainK);

		System.out.print("\t");
		
		if(seg<maxS-1)
			System.out.print(seg);
		else System.out.print("*");

		for(int i = 0; i<oinezKop+1; i++)System.out.print("\t");
		
			
		System.out.println();


	}




	private void kopuruaInprimatu(int kop, int max){

		System.out.print("[");
		for(int i=0; i<kop; i++)
			System.out.print("*");
		for(int i=kop; i<max; i++)
			System.out.print(" ");

		System.out.print("]");

	}
	private void kopuruaInprimatuK(int kop, int max){

		System.out.print("[");
		for(int i=0; i<kop; i++)
			System.out.print("k");
		for(int i=kop; i<max; i++)
			System.out.print(" ");

		System.out.print("]");

	}

	private void kopuruaInprimatuKseg(int kop, int max){

		System.out.print("[");

		for(int i=0; i<max; i++)
			if(i<kop)
				System.out.print("k");
			else
				System.out.print(" ");

		System.out.print("]");

	}


	private void kopuruaInprimatuO(int kop, int max){

		System.out.print("[");
		for(int i=0; i<kop; i++)
			System.out.print("o");
		for(int i=kop; i<max; i++)
			System.out.print(" ");

		System.out.print("]");

	}
}
