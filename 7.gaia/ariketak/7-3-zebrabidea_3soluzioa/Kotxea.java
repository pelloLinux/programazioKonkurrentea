
public class Kotxea extends Thread {

	private int izena;
	private Zebrabidea zebra;
	Kotxea(int iz, Zebrabidea z){setIzena(iz); zebra=z;}

	//KOTXEA = (iritsi -> sartu -> atera -> KOTXEA).
	public void run() {

		try {

			while(true)
			{

				sleep((int)(Math.random()*3000));
				zebra.kotxeaIritsi(this);
				sleep((int)(Math.random()*1000));
				zebra.kotxeaSartu(this);
				sleep((int)(Math.random()*1000));
				zebra.kotxeaAtera(this);
				sleep((int)(Math.random()*30000));

			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int getIzena() {
		return izena;
	}

	public void setIzena(int izena) {
		this.izena = izena;
	}
}
