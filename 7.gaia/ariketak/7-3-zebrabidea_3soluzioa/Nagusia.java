
public class Nagusia {

	/**
	 * 6.3.zebrabidea
	 * 
		3. Zebrabidea:
		• Zebrabide batetara iristean, kotxeak zain geldituko dira oinezkoren bat pasatzen
		edo pasatzeko zain baldin badago.
		• Oinezkoek, ordea, zain geldituko dira une horretan kotxe bat pasatzen ari bada,
		harrapatuak ez izateko.
		• Gainera ataskorik sor ez dadin, hiru kotxe baino gehiago zain badaude,
		oinezkoek ere itxarongo dute.
	 * 
	 * Pello Arrue
	 * 
	 *2016/11/14
	 */


	//3.soluzioa
	//Soluzioa => Segidan pasatu daitezkeen kotxe kopurua mugatzea
	
//	||NAGUSIA = (k[RK]:KOTXEA || o[RO]:OINEZKOA || ZEBRABIDEA).
//  ||NAGUSIA2 = (NAGUSIA)<<{k[RK].iritsi, o[RO].iritsi}.
	
	
	public static void main(String[] args) {

		
		int kotxeKop = 6;
		int oinezkoKop = kotxeKop;//kotxe kopurua eta oinezko kopurua berdinak beti
		int max = 5;
		Pantalla p = new Pantalla(oinezkoKop, kotxeKop);
		Zebrabidea z = new Zebrabidea(p, max);
		
		Kotxea[] kotxeak = new Kotxea[kotxeKop];
		for(int i=0; i<kotxeKop; i++)
			kotxeak[i]=new Kotxea(i+1, z);
		
		Oinezkoa[] oinezkoak = new Oinezkoa[kotxeKop];
		for(int i=0; i<oinezkoKop; i++)
			oinezkoak[i]=new Oinezkoa(i+1, z);
		
		p.goiburukoaInprimatu(max);
		
		
		for(int i=0; i<kotxeKop; i++)
			kotxeak[i].start();
		for(int i=0; i<oinezkoKop; i++)
			oinezkoak[i].start();
		
	}

}
