
public class Zebrabidea {
	
	public int zebraK;
	public int zebraO;
	public int zainK;
	public int zainO;
	public int segK;
	public int maxSeg;
	private Pantalla p;
	
	Zebrabidea(Pantalla p, int max) {this.p = p; this.maxSeg = max;}
	
	//k[RK].iritsi	->ZEBRABIDEA[zebraK][zebraO][zainK+1][zainO][segK]
	public synchronized void kotxeaIritsi(Kotxea k)
	{
		zainK++;
		p.inprimatuKotx(k.getIzena()-1, "iritsi", this,segK,maxSeg);
		notifyAll();
	}
	//o[RO].iritsi	->ZEBRABIDEA[zebraK][zebraO][zainK][zainO+1][segK]
	public synchronized void oinezkoaIritsi(Oinezkoa o)
	{
		zainO++;
		p.inprimatuOinez(o.getIzena()-1, "iritsi", this,segK,maxSeg);
		notifyAll();
	}
	//|when(zebraO==0 && (zainO==0 || (seg+1<KM && zainK>ZM)))			k[RK].sartu		-> ZEBRABIDEA[zebraK+1][zebraO][zainK-1][zainO][seg+1]
	public synchronized void kotxeaSartu(Kotxea k) throws InterruptedException
	{
		while(!(zebraO==0 &&(zainO==0 ||  segK+1<maxSeg && zainK>3)))wait();
		
		zebraK++;
		zainK--;
		segK++;	
		p.inprimatuKotx(k.getIzena()-1, "sartu", this,segK,maxSeg);	
		notifyAll();
	}
	//when(zebraK==0 && (zainK<=3 || segK+1>=N))				o[RO].sartu		-> ZEBRABIDEA[zebraK][zebraO+1][zainK][zainO-1][0]
	public synchronized void oinezkoaSartu(Oinezkoa o) throws InterruptedException
	{
		while(!(zebraK==0 && (zainK<=3 || segK+1>=maxSeg)))wait();
		
		zebraO++;
		zainO--;
		segK = 0;
		p.inprimatuOinez(o.getIzena()-1, "sartu", this,segK,maxSeg);
		notifyAll();
	}
	
	//k[RK].atera		->ZEBRABIDEA[zebraK-1][zebraO][zainK][zainO][segK]
	public synchronized void kotxeaAtera(Kotxea k)
	{
		zebraK--;
		p.inprimatuKotx(k.getIzena()-1, "atera", this,segK,maxSeg);
		notifyAll();
	}
	//o[RO].atera		->ZEBRABIDEA[zebraK][zebraO-1][zainK][zainO][segK]
	public synchronized void oinezkoaAtera(Oinezkoa o)
	{
		zebraO--;
		p.inprimatuOinez(o.getIzena()-1, "atera", this,segK,maxSeg);
		notifyAll();
	}
}
