

public class nagusia {

	/**
	 * 5.10.lifo
	 * 
	 * 10. LIFO ilara batean prozesuak sartu eta ateratzen dira.
	 * 
	 * Pello Arrue
	 * 
	 *2016/09/30
	 */
	
	//||NAGUSIA = (p[PR]:P || p[PR]::FIFO).
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int prozesuKop = 10;
		int fifoMax = 8;
		int sarMax = 15;
		
		//pantalla sortu
		pantalla p = new pantalla(prozesuKop);
		//lifoa sortu
		lifo lifo = new lifo(fifoMax,sarMax, p);
		//prozesuak sortu
		prozesu[] prozesuak = new prozesu[prozesuKop];
		for(int i = 0; i<prozesuKop; i++)
			prozesuak[i] = new prozesu(i, lifo);
		//goiburukoa inpriomatu
		p.goiburukoaInprimatu(prozesuKop, "lifo", "sarketak",sarMax);
		//ariak hasieratu
		for(int i = 0; i<prozesuKop; i++)
			prozesuak[i].start();
		

	}

}
