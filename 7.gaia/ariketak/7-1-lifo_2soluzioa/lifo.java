

public class lifo {
	

	private int kop;
	private int sar;
	private int sarMax;
	private int max;
	private pantalla pantalla;
	
	//eraikitzailea
	lifo(int k, int sMax, pantalla p){

		kop = 0;
		sar = 0;
		max = k;
		sarMax = sMax;
		pantalla = p;
	}
	
	//when(k<FK && s<N)	sartu_return[k]		-> LIFO [k+1][s+1]
	public synchronized int sartu(prozesu p) throws InterruptedException{
		
		while (!(kop<max && sar<sarMax))wait();

		kop++;
		sar++;
		notifyAll();
		pantalla.inprimatu("sartu", kop, p.getIzena(), kop, max,sar,sarMax);
		return kop;
		
	}
	//atera[k-1]			-> if(k-1!=0)then LIFO [k-1][s]	
 //	                           else LIFO [k-1][0]
	public synchronized void atera(prozesu p,int prozInd) throws InterruptedException{
		
		while(!(prozInd==kop))wait();
		pantalla.inprimatu("atera", kop, p.getIzena(), kop-1, max, sar, sarMax);
		kop--;
		if(kop==0)sar = 0;
		notifyAll();
	}

}

