


class pantalla {

	private int kopPosizioak;

	public pantalla(int kopPosizioak){
		this.kopPosizioak=kopPosizioak;
	}

	public void goiburukoaInprimatu(int kop, String iz2, String iz3, int max){


		for(int i=1; i<=kop; i++){
			System.out.print("p["+i+"]");
			System.out.print("\t");
		}
		System.out.print("\t");
		System.out.print(iz2);
		System.out.print("\t");
		System.out.print("\t");
		System.out.print("\t");
		System.out.println(iz3+"(max["+max+"])");

		System.out.print("============================");
		for(int i=1; i<=kop; i++)
			System.out.print("===================");
		System.out.println();


	}
	public synchronized void inprimatu(String s,int ind, int pos, int kop, int kopMax, int kopS, int maxS){


		//espazioak inprimatu
		for(int i=0; i<pos; i++)System.out.print("\t");
		System.out.print(s+"["+ind+"]");//arazoa honek ematen du(bi zifrako zenbakiek)
		for(int i=0; i<this.kopPosizioak-pos-1; i++)System.out.print("\t");
		System.out.print("\t");
		kopuruaInprimatu(kop,kopMax);
		System.out.print("\t");
		if(kopS<maxS)
			System.out.print(kopS);
		else
			System.out.print("*");
		System.out.println();
		notify();
	}

	private void kopuruaInprimatu(int kop, int kopMax){

		System.out.print("|");
		for(int i=0; i<kop; i++)
			System.out.print("*|");
		for(int i=kop; i<kopMax; i++)
			System.out.print(" |");
	
		
	}

	public int getKopPosizioak() {
		return kopPosizioak;
	}

	public void setKopPosizioak(int kopPosizioak) {
		this.kopPosizioak = kopPosizioak;
	}

}