/**
 * 7.2. tutoretzak
 * 
 * 2. Konkurrenteko ikasleek azterketa garaian ikasten egoten dira (suposatzen da):
		• Ikasleek zerbait ez badute ulertzen irakaslearen bulegora joaten dira, bulegoan
		sartu, galdera egin eta erantzuna jaso ondoren bulegotik ateratzen dira, berriz
		ikastera joateko.
		• Irakasleak, ikasle batek galdera bat egiten dionean, erantzun baino lehen
		pentsatu egiten du (suposatzen da).
		• Gainera irakaslea nahiko berezia denez, tutoretzetarako ondoko arauak ditu:
		◦ Bulegoan bi ikasle batera egon daitezke, baina ez gehiago.
		◦ Ikasle batek egin duen galdera erantzun arte, besteak ezin du galdetu.
		◦ Ikasle bakoitzak galdera bakar bat egin dezake tutoretza bakoitzean.
		◦ Bulegoan sartzeko eta galdera egiteko ez da errespetatzen aurretik zein
		zegoen.
 * 
 * Pello Arrue
 * 
 *2016/11/18
 */

//||NAGUSIA = ( TUTORETZA || ikas[IR]:IKASLEA || ira:IRAKASLEA).

public class Nagusia {

	public static void main(String[] args) {

		int ikasleKop = 4;
		pantalla p = new pantalla(ikasleKop+2);
		Tutoretza t = new Tutoretza(p);
		Irakaslea ir = new Irakaslea(0,t,p);

		Ikaslea[] ikasleak = new Ikaslea[ikasleKop];
		for(int i=0; i<ikasleKop; i++)
			ikasleak[i]=new Ikaslea(i+1, t, p);
		
		p.goiburukoaInprimatu("Irakaslea",ikasleKop,"tutoretza","Egoera");
		
		ir.start();
		for(int i=0; i<ikasleKop; i++)
			ikasleak[i].start();
		

	}

}
