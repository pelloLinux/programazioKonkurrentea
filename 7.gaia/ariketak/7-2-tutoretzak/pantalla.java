
class pantalla {

	private int kopPosizioak;

	public pantalla(int kopPosizioak){
		this.kopPosizioak=kopPosizioak;
	}

	public void goiburukoaInprimatu(String iz1, int kop, String iz2, String iz3){

		System.out.print(iz1);
		System.out.print("\t");
		for(int i=1; i<=kop; i++){
			System.out.print("i["+i+"]");
			System.out.print("\t\t");
		}
		System.out.print(iz2);
		System.out.print("\t\t");
		System.out.println(iz3);
		System.out.print("==================================================");
		for(int i=1; i<=kop; i++)
			System.out.print("================");
		System.out.println();


	}
	public synchronized void inprimatu(String s, int pos, int kop, int max, int egoera){


		//espazioak inprimatu
		for(int i=0; i<pos; i++)System.out.print("\t\t");
		System.out.print(s);
		for(int i=0; i<this.kopPosizioak-pos-1; i++)System.out.print("\t\t");
		kopuruaInprimatu(kop,max);
		System.out.print("\t\t");
		if(egoera == 0)
			System.out.println("Libre");
		else if(egoera == 1)
			System.out.println("Galdera Jasota");
		else if(egoera == 3)
			System.out.print("Galdera Erantzunda");
		
		System.out.println();
		notify();
	}
	
	public synchronized void inprimatu2(String s, int pos){


		//espazioak inprimatu
		for(int i=0; i<pos; i++)System.out.print("\t\t");
		System.out.print(s);
		for(int i=0; i<this.kopPosizioak-pos-1; i++)System.out.print("\t\t");
		//kopuruaInprimatu(kop,max);
		System.out.println();
		notify();
	}

	private void kopuruaInprimatu(int kop, int max){

		System.out.print("[");
		for(int i=0; i<kop; i++)
			System.out.print("*");
		for(int i=kop; i<max; i++)
			System.out.print(" ");
		
		System.out.print("]");


	}

	public int getKopPosizioak() {
		return kopPosizioak;
	}

	public void setKopPosizioak(int kopPosizioak) {
		this.kopPosizioak = kopPosizioak;
	}

}