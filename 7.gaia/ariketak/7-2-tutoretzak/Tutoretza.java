

public class Tutoretza {

	int i;
	int max;
	int t;
	int zeinenGaldera;
	pantalla pant;

	Tutoretza(pantalla p){
		max = 2;
		i = 0;
		t = 0;
		pant = p;

	}

	// when(i<2)	 ikas[IR].bulegoanSartu	-> TUTORETZA[i+1][t]
	public synchronized void bulegoanSartu(Ikaslea ikas) throws InterruptedException{

		while(!(i<2))wait();
		i++;
		pant.inprimatu("bulegoanSartu", ikas.izena, i, max,t);
		notifyAll();

	}
	//|when(t==0)  ikas[IR].galderaEgin -> TUTORETZA[i][1]
	public synchronized void galderaEgin(Ikaslea ikas) throws InterruptedException
	{
		while(!(t==0))wait();
		t=1;
		zeinenGaldera = ikas.izena;
		pant.inprimatu("galderaEgin", ikas.izena, i, max,t);
		notifyAll();
	}
	//|when(t==1)  ira.galderaJaso 	-> TUTORETZA[i][2]
	public synchronized void galderaJaso(Irakaslea irak) throws InterruptedException
	{
		while(!(t==1))wait();
		t=2;
		pant.inprimatu("galderaJaso["+zeinenGaldera+"]", 0, i, max,t);
		notifyAll();
	}
	//ira.erantzun 	-> TUTORETZA[i][3]
	public synchronized void erantzun(Irakaslea irak) throws InterruptedException
	{
		t=3;
		pant.inprimatu("erantzun["+zeinenGaldera+"]", 0, i, max,t);
		notifyAll();
	}
	//|when(t==3)	 ikas[IR].erantzunaJaso -> TUTORETZA[i][0]
	public synchronized void erantzunaJaso(Ikaslea ikas) throws InterruptedException
	{
		while(!(t==3))wait();
		t=0;
		pant.inprimatu("ErantzunaJaso", ikas.izena, i, max,t);
		notifyAll();
	}
	//ikas[IR].ikasteraJoan -> TUTORETZA[i-1][t]
	public synchronized void ikasteraJoan(Ikaslea ikas) throws InterruptedException{

		i--;
		pant.inprimatu("IkasteraJoan", ikas.izena, i, max,t);
		notifyAll();

	}
}
