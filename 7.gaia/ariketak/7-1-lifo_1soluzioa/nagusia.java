

public class nagusia {

	/**
	* 7.1.lifo
	 * 
	 * 1. LIFO pilaren arazoa, prozesu guztiak noizbait pilatik aterako direla ziurtatuz.
	 * 
	 * Pello Arrue
	 * 
	 *2016/11/18
	 */
	 
	
	//||NAGUSIA = (p[PR]:P || p[PR]::FIFO).
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int prozesuKop = 15;
		int fifoMax = 8;
		//pantalla sortu
		pantalla p = new pantalla(prozesuKop);
		//lifoa sortu
		lifo lifo = new lifo(fifoMax, p);
		//prozesuak sortu
		prozesu[] prozesuak = new prozesu[prozesuKop];
		for(int i = 0; i<prozesuKop; i++)
			prozesuak[i] = new prozesu(i, lifo);
		//goiburukoa inpriomatu
		p.goiburukoaInprimatu(prozesuKop, "fifo");
		//ariak hasieratu
		for(int i = 0; i<prozesuKop; i++)
			prozesuak[i].start();
		

	}

}
