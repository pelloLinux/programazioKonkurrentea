

public class lifo {
	

	private int kop;
	private int max;
	private pantalla pantalla;
	
	//eraikitzailea
	lifo(int k, pantalla p){

		kop = 0;
		max = k;
		pantalla = p;
	}
	
	//when(k<FK)	sartu_return[k]		-> LIFO [k+1]
	public synchronized int sartu(prozesu p) throws InterruptedException{
		
		while (!(kop<max))wait();

		kop++;
		notifyAll();
		pantalla.inprimatu("sartu", kop, p.getIzena(), kop, max);
		return kop;
		
	}
	//			atera[k-1]			-> LIFO [k-1]	
	public synchronized void atera(prozesu p,int prozInd) throws InterruptedException{
		
		while(!(prozInd==kop))wait();
		pantalla.inprimatu("atera", kop, p.getIzena(), kop-1, max);
		kop--;
		notifyAll();
	}

}

