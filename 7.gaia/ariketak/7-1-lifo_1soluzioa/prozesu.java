
public class prozesu extends Thread {

	private int izena;
	//	private int gordetakoInd;
	private lifo lifo;

	//eraikitzailea
	prozesu(int iz, lifo f){
		setIzena(iz);
		//gordetakoInd = -1;
		lifo = f;
	}

	//P = (sartu_return[i:FR] -> atera[i]-> P).
	public void run() {

		try {

			while(true)
			{
				//sartu_return[i:FR]
				int gordetakoInd = lifo.sartu(this);
				sleep((int)(Math.random()*1000+500));
				//atera[i]
				lifo.atera(this,gordetakoInd);
				sleep((int)(Math.random()*500));

			}


		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getIzena() {
		return izena;
	}

	public void setIzena(int izena) {
		this.izena = izena;
	}

}
