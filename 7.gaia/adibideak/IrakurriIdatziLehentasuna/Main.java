
public class Main {


	public static void main (String args[]) {
		IrakurriIdatziLehentasuna seg = new IrakurriIdatziLehentasuna();
		int IdKop=3;
		int IrKop=5;
		String tabul="";

		for(int i=1; i<=IrKop;i++){ System.out.print(i+".irak\t");       }    
		for(int i=1; i<=IdKop;i++){ System.out.print("\t"+i+".idaz");  }    
		System.out.println("\n==========================================================\n");

		Irakurlea ir[] = new Irakurlea[IrKop];
		Idazlea id[] = new Idazlea[IdKop];

		for(int i=0; i<IrKop;i++){
			ir[i] = new Irakurlea(seg,tabul,1,3);
			ir[i].start();   
			tabul=tabul+"\t";
		}

		for(int i=0; i<IdKop;i++){
			tabul=tabul+"\t";
			id[i] = new Idazlea(seg,tabul,1,3);
			id[i].start();
		}
	}
}

