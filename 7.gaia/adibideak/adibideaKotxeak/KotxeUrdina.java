package kotxeak;

class KotxeUrdina extends Thread{
	Zubia zubia; Pantaila pantaila; int zenb;
	KotxeUrdina(Zubia b, Pantaila p, int zenb) {
		this.zenb = zenb; zubia = b; pantaila = p; }
	public void run() {
		try {
			while(true) {
				while (!pantaila.mugituUrdina(zenb)); // mugitu zubitik kanpo
				zubia.sartuUrdina();
				// eskatzen du zubiaren atzipena
				while (pantaila.mugituUrdina(zenb)); // mugitu zubian
				zubia.irtenUrdina();
				// askatzen du zubiaren atzipena
			}
		} catch (InterruptedException e){}
	}
}