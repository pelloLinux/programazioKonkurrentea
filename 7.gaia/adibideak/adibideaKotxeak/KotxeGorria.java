package kotxeak;

class KotxeGorria extends Thread{
	Zubia zubia; Pantaila pantaila; int zenb;
	KotxeGorria(Zubia b, Pantaila p, int zenb) {
		this.zenb = zenb; zubia = b; pantaila = p; }
	public void run() {
		try {
			while(true) {
				while (!pantaila.mugituGorria(zenb)); // mugitu zubitik kanpo
				zubia.sartuGorria();
				// eskatzen du zubiaren atzipena
				while (pantaila.mugituGorria(zenb)); // mugitu zubian
				zubia.irtenGorria();
				// askatzen du zubiaren atzipena
			}
		} catch (InterruptedException e){}
	}
}