package kotxeak;

public class ZubiaApp {
	public static void main (String args[]) {
		int maxGorri = 2;
		int maxUrdin = 7;
		int zabalera = 11;
		int zubezk=zabalera/2;
		int zubesk=(zabalera/2)+1;
		KotxeGorria[] gorria= new KotxeGorria[maxGorri];
		KotxeUrdina[] urdina= new KotxeUrdina[maxUrdin];
		Pantaila p= new Pantaila(maxGorri,maxUrdin, zabalera, zubezk, zubesk);
		Zubia z;
		z = new Zubia();
		//z = new ZubiSegurua();

		for (int i = 0; i<maxGorri; i++) 
			gorria[i] = new KotxeGorria(z,p,i);		
		for (int i = 0; i<maxUrdin; i++) 
			urdina[i] = new KotxeUrdina(z,p,i); 
		
		for (int i = 0; i<maxGorri; i++) 
			gorria[i].start();
		for (int i = 0; i<maxUrdin; i++) 
			urdina[i].start(); 
	}

}