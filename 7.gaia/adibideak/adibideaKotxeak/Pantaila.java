package kotxeak;

class Pantaila {
	int zab, maxUrdin, maxGorri, zubezk, zubesk;
	int[] gorriaX,urdinaX;
	String[] tabul;


	Pantaila(int m1, int m2, int zabalera, int ezk, int esk) {
		maxGorri = m1;
		maxUrdin = m2;
		zubezk=ezk;
		zab = zabalera; zubesk=esk;
		gorriaX = new int[maxGorri];
		urdinaX = new int[maxUrdin];


		//Kotxe bakoitzaren hasierako posizioa
		for (int i = 0; i<maxGorri ; i++) 
			gorriaX[i] = i;
		for (int i = 0; i<maxUrdin ; i++) 
			urdinaX[i] = zab-i;
		
		//Tabulazioak
		tabul = new String[zab+1];
		for (int i=0; i<zab+1; ++i){
			tabul[i]="";
			for (int j=0; j<i; ++j) tabul[i]=tabul[i]+"\t";
		}
		pantailaratu();

	}

	boolean mugituGorria(int i) throws InterruptedException {
		int X = gorriaX[i];
		synchronized (this) {
			if
			(X==zab && gorriaX[(i+1)%maxGorri]!=0)
				X=0;
			//Bukaerara iristean
			else if (X!=zab && gorriaX[(i+1)%maxGorri]!=X+1) X=X+1; //Beste posizioetan
			if (gorriaX[i]!=X) {
				//Mugitu ahal bada, mugitu eta pantailaratu
				gorriaX[i]=X; pantailaratu();}
		}
		try{Thread.sleep(2000);} catch(InterruptedException e) {}
		return (X>=zubezk-1 && X<=zubesk);
		//Zubira sartzeko edo zubian badago
	}
	
	
	boolean mugituUrdina(int i) throws InterruptedException{ 

		int X = urdinaX[i];
		synchronized (this) {
			if
			(X==0 && urdinaX[(i+1)%maxUrdin]!=0)
				X=zab;
			//Bukaerara iristean
			else if (X!=0 && urdinaX[(i+1)%maxUrdin]!=X-1) X=X-1; //Beste posizioetan
			if (urdinaX[i]!=X) {
				//Mugitu ahal bada, mugitu eta pantailaratu
				urdinaX[i]=X; pantailaratu();}
		}
		try{Thread.sleep(2000);} catch(InterruptedException e) {}
		return (X>=zubezk && X<=zubesk+1);
		//Zubira sartzeko edo zubian badago
	}
	void pantailaratu() {
		// Gorriak
		for (int i = 0; i<maxGorri ; i++)
			System.out.println(gorriaX[i]+"|\t"+tabul[gorriaX[i]]+"gorri"+i+">>");
		// Urdinak
		for (int i = 0; i<maxUrdin ; i++)
			System.out.println(urdinaX[i]+"|\t"+tabul[urdinaX[i]]+"<<urdin"+i);
		// Zubia
		System.out.print("\t"+tabul[zubezk]);
		for (int i = zubezk; i<zubesk+1 ; i++) System.out.print("********");
		System.out.println();
	}

}
