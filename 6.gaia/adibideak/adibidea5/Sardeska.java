package filosofoak;

class Sardeska {
	private boolean hartua=false;
	private int zenbakia;
	private Pantalla p;

	Sardeska(int zenb, Pantalla pan){zenbakia = zenb;p=pan;}

	synchronized void put() {
		hartua=false;
		System.out.println(zenbakia+" utzia |");
		notify();
	}

	synchronized void get()throws java.lang.InterruptedException {

		while (hartua) wait();
		hartua=true;
		p.inpr(zenbakia+" hartua|");
	}
}