package filosofoak;

public class Filosofoa extends Thread {

	int izena;
	String tartea;
	Sardeska eskubikoa, ezkerrekoa;
	private Pantalla p;
	
	
	Filosofoa(int a, Sardeska c, Sardeska d,  String b, Pantalla pan){
		
		izena = a;
		eskubikoa = c;
		ezkerrekoa = d;
		tartea = b;
		p = pan;
		
	}

	public void run() {

		try {
			while (true) {
				
				System.out.println(tartea+" pentsatzen");
				sleep((int)(1000*Math.random()));
				p.inpr(tartea+" gose");
				eskubikoa.get();
				p.inpr(tartea+" eskub.hartu"); 
				sleep(500);
				ezkerrekoa.get();
				p.inpr(tartea+" ezker.hartu");
				p.inpr(tartea+" jaten");
				sleep((int)(500*Math.random()));
				eskubikoa.put();
				ezkerrekoa.put();
			}
		} catch (java.lang.InterruptedException e) {}
	}
}

