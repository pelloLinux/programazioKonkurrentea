

public class Filosofoa extends Thread {

	int izena;
	String tartea;
	Sardeska eskubikoa, ezkerrekoa;
	private Pantalla p;


	Filosofoa(int a, Sardeska c, Sardeska d,  String b, Pantalla pan){

		izena = a;
		eskubikoa = c;
		ezkerrekoa = d;
		tartea = b;
		p = pan;

	}



	public void run() {

		try {
			while (true) {

				System.out.println(tartea+" pentsatzen");
				sleep((int)(5000*Math.random()));
				p.inpr(tartea+" gose");
				if(eskubikoa.get())
				{
					p.inpr(tartea+" eskub.hartu"); 
					sleep(500);
					if(ezkerrekoa.get())
					{
						p.inpr(tartea+ " ezker.hartu");
						p.inpr(tartea+" jaten");
						sleep((int)(10000*Math.random()));
						eskubikoa.put();
						p.inpr(tartea+ " eskub.laga");
						ezkerrekoa.put();
						p.inpr(tartea+ " ezker.laga");
						
					}
					else{
						p.inpr(tartea+ " zain aspertu");
						eskubikoa.put();
						p.inpr(tartea+ " eskub.laga");

					}
				}else
					p.inpr(tartea+ " zain aspertu");

			}
		} catch (java.lang.InterruptedException e) {}
	}
}

