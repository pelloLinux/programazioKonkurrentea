/**
 * 6.3.filosofoak1
 * 
 * 3. Javaren wait primitiba erabiliz Sardeska monitorearen inplementazioa aldatu,
	segundu bat zain egon ondoren get deia itzultzeko false balioarekin. 
	Filosofoak beste sardeskarik bazuen askatu beharko du eta berriz saiatu. 
	Aztertu sistemaren jokaera.
 * 
 * Pello Arrue
 * 
 *2016/11/05
 */


class FilosofoApp {
    final static int Kop = 7;
    final static Filosofoa[] fil = new Filosofoa[Kop];
    final static Sardeska[] sar = new Sardeska[Kop];
    final static String[] tartea = new String[Kop];
 
    public static void main (String args[]){
        tartea[0]="\t\t";
        for (int i=1; i<Kop; ++i) tartea[i]=tartea[i-1]+"\t\t"; 
        for (int i=0; i<Kop; ++i) System.out.print( "\t\t    "+i);
        System.out.println();
        Pantalla p = new Pantalla();
        
        for (int i=0; i<Kop; ++i)
            sar[i] = new Sardeska(i,p);
        for (int j=0; j<Kop; ++j){
            fil[j] = new Filosofoa(j, sar[(j-1+Kop)%Kop], sar[j], tartea[j],p);
            fil[j].start();
        }
      }
}
