

class Sardeska {
	private boolean hartua=false;
	private int zenbakia;
	private Pantalla p;

	Sardeska(int zenb, Pantalla pan){zenbakia = zenb;p=pan;}


	synchronized void put() {
		hartua=false;
		p.inpr(zenbakia+" utzia |");
		notify();
	}


	synchronized boolean get()throws java.lang.InterruptedException {


		if (!hartua)
		{
			hartua=true;
			p.inpr(zenbakia+" hartua|");
			return true;
		}
		//segundu bat itxaron
		wait(1000);
		
		if (!hartua)
		{
			hartua=true;
			p.inpr(zenbakia+" hartua|");
			return true;
		}
		else
			return false;
	}
}
