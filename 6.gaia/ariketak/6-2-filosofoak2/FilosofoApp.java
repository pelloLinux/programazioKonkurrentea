/**
 * 6.2.filosofoak2
 * 
 * 2.
		Filosofoen afarirako beste soluzio bat: soilik lau filosofo eser daitezke batera.
		MAIORDOMO prozesu bat espezifikatu, proposatutako ereduarekin konposatzean,
		gehienez lau filosofo eseri ekintza egitea baimentzen duena, altxatu ekintza bat
		gertatu aurretik. Frogatu ez dela elkar-blokeaketarik ematen. 
		FSPz modelatu eta Javaz inplementatu.
 * 
 * Pello Arrue
 * 
 *2016/11/05
 */

//||AFARIA = forall[i:FR](fil[i]:FIL(i) || {fil[i].ezker,fil[((i-1)+N)%N].eskubi}::SARD || MAHAIA).

class FilosofoApp {
    final static int Kop = 5;
    final static Filosofoa[] fil = new Filosofoa[Kop];
    final static Sardeska[] sar = new Sardeska[Kop];
    final static String[] tartea = new String[Kop];
 
    public static void main (String args[]){
        tartea[0]="\t\t";
        for (int i=1; i<Kop; ++i) tartea[i]=tartea[i-1]+"\t\t"; 
        for (int i=0; i<Kop; ++i) System.out.print( "\t\t    "+i);
        System.out.print( "\t\t Mahaia");
        System.out.println();
        for (int i=0; i<Kop; ++i) System.out.print( "====================");
        System.out.println();
        Pantalla p = new Pantalla();
        mahaia m = new mahaia(Kop,Kop,p);
        
        for (int i=0; i<Kop; ++i)
            sar[i] = new Sardeska(i,p);
        for (int j=0; j<Kop; ++j){
            fil[j] = new Filosofoa(j, sar[(j-1+Kop)%Kop], sar[j], tartea[j],p,m);
            fil[j].start();
        }
      }
}