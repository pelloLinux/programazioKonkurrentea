

public class Filosofoa extends Thread {

	int izena;
	String tartea;
	Sardeska eskubikoa, ezkerrekoa;
	mahaia mahaia;
	private Pantalla p;


	Filosofoa(int a, Sardeska c, Sardeska d,  String b, Pantalla pan, mahaia m){

		izena = a;
		eskubikoa = c;
		ezkerrekoa = d;
		tartea = b;
		p = pan;
		mahaia = m;

	}

	
	//eseri -> ezker.get->eskubi.get->jan->ezker.put->eskubi.put-> altxatu-> FIL
	public void run() {

		try {
			while (true) {

				p.inpr(tartea+" EseriNahi");
				mahaia.eseri(this);
				System.out.println(tartea+" pentsatzen");
				sleep((int)(1000*Math.random()));
				p.inpr(tartea+" gose");
				eskubikoa.get();
				p.inpr(tartea+" eskub.hartu"); 
				sleep(500);
				ezkerrekoa.get();
				p.inpr(tartea+" ezker.hartu");
				p.inpr(tartea+" jaten");
				sleep((int)(500*Math.random()));
				eskubikoa.put();
				ezkerrekoa.put();
				mahaia.altxaxtu(this);
			}
		} catch (java.lang.InterruptedException e) {}
	}
}

