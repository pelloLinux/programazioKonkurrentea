
public class mahaia {

	private int kopDenera;
	private int filosofoKop;
	private int maxKop;
	private Pantalla pant;
	
	mahaia(int kopD,int max, Pantalla p){maxKop = max; pant = p; kopDenera = kopD;}
	
	//when (i<N-1)fil[FR].eseri -> MAHAIA[i+1]
	public synchronized void eseri(Filosofoa f) throws InterruptedException{
		while(!(filosofoKop<maxKop-1))wait();
		pant.inpr(f.tartea+" eseri");
		filosofoKop++;
		pant.inpr2(kopDenera, filosofoKop, maxKop);
		notifyAll();
	}
	
	//fil[FR].altxatu -> MAHAIA[i-1]
	public synchronized void altxaxtu(Filosofoa f){
		filosofoKop--;
		pant.inpr(f.tartea+" altxatu");
		pant.inpr2(kopDenera, filosofoKop, maxKop);
		notifyAll();
	}
}
