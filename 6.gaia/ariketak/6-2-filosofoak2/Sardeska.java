

class Sardeska {
	private boolean hartua=false;
	private int zenbakia;
	private Pantalla p;

	Sardeska(int zenb, Pantalla pan){zenbakia = zenb;p=pan;}

	//when (h==1) put->SARD[0]).
	synchronized void put() {
		hartua=false;
		System.out.println(zenbakia+" utzia |");
		notify();
	}

	//when (h==0) get->SARD[1]
	synchronized void get()throws java.lang.InterruptedException {

		while (hartua) wait();
		hartua=true;
		p.inpr(zenbakia+" hartua|");
	}
}