

public class Filosofoa extends Thread {

	int izena;
	String tartea;
	Sardeska eskubikoa, ezkerrekoa;
	private Pantalla p;


	Filosofoa(int a, Sardeska c, Sardeska d,  String b, Pantalla pan){

		izena = a;
		eskubikoa = c;
		ezkerrekoa = d;
		tartea = b;
		p = pan;

	}

	
//	FIL(I=0) =  ( when (I%2==0) ezker.get->eskubi.get->jan->ezker.put->eskubi.put->FIL
//				| when (I%2==1) eskubi.get->ezker.get->jan->ezker.put->eskubi.put->FIL ).
	
	public void run() {

		try {
			while (true) {

				System.out.println(tartea+" pentsatzen");
				sleep((int)(1000*Math.random()));
				p.inpr(tartea+" gose");
				
				if(izena%2==0){
					
					ezkerrekoa.get();
					p.inpr(tartea+" ezker.hartu");
					sleep((int)(1000*Math.random()));
					eskubikoa.get();
					p.inpr(tartea+" eskub.hartu"); 
					sleep((int)(500*Math.random()));
					p.inpr(tartea+" jaten");
					sleep((int)(500*Math.random()));
					ezkerrekoa.put();
					p.inpr(tartea+" ezker.utzi");
					sleep((int)(500*Math.random()));
					eskubikoa.put();
					p.inpr(tartea+" eskuin.utzi");
					
					
				}
				else if (izena%2==1){
					
					eskubikoa.get();
					p.inpr(tartea+" eskub.hartu"); 
					sleep((int)(500*Math.random()));
					ezkerrekoa.get();
					p.inpr(tartea+" ezker.hartu");
					sleep((int)(1000*Math.random()));		
					p.inpr(tartea+" jaten");
					sleep((int)(500*Math.random()));
					eskubikoa.put();
					p.inpr(tartea+" eskuin.utzi");
					sleep((int)(500*Math.random()));
					ezkerrekoa.put();
					p.inpr(tartea+" ezker.utzi");
					
				}
				sleep((int)(500*Math.random()));
				
			}
		} catch (java.lang.InterruptedException e) {}
	}
}

