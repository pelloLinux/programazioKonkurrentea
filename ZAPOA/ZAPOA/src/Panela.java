

import java.awt.Image;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.Timer;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent; 

public class Panela extends JPanel implements ActionListener {

	private Image[] image = new Image[KotxeAnimatuak.k+KotxeAnimatuak.kop];
	private Image background;
	private Timer timer;
	private int[] x = new int[KotxeAnimatuak.k+KotxeAnimatuak.kop];
	private int[] y = new int[KotxeAnimatuak.k+KotxeAnimatuak.kop];

	//x ardatzean kokatu kotxea
	public void setXkotxea(int kotxea, int balioa){

		for(int i=0; i<KotxeAnimatuak.k; i++)
			if(kotxea==i+1)
				x[i] = balioa;
	}
	//x ardatzean ZAPOA
	public void setXZapoa(int id,boolean gora, int A){

		if(gora)
			x[KotxeAnimatuak.k+id]+=A;
		else 
			x[KotxeAnimatuak.k+id]-=A;


	}
	//y ardatzean ZAPOA
	public void setYZapoa(int id,boolean gora, int A){

		if(gora)
			y[KotxeAnimatuak.k+id]-=A;
		else 
			y[KotxeAnimatuak.k+id]+=A;

	}
	public void hasieraZapoa(Zapoa z){

		x[KotxeAnimatuak.k+z.izena]=z.xhasiera;
		y[KotxeAnimatuak.k+z.izena]=z.yframe;

	}
	public void setImage(Kotxea k){

		ImageIcon ii = new ImageIcon(this.getClass().getResource(k.getIrudia()));
		image[k.getKotxea()-1] = ii.getImage();
	}
	public void setImageZapoa(int id){

		ImageIcon ii = new ImageIcon(this.getClass().getResource("Imagenak/gorri.png"));
		image[KotxeAnimatuak.k+id] = ii.getImage();
	}

	public void setGameOver()
	{
		for(int i=0; i<KotxeAnimatuak.k; i++)
		{
			ImageIcon ii;
			ii = new ImageIcon(this.getClass().getResource("Imagenak/gm.png"));
			image[i] = ii.getImage();
		}


	}
	public void setIrabazi(int id)
	{
		if(id == 0)
		{
			for(int i=0; i<KotxeAnimatuak.k; i++)
			{
				ImageIcon ii;
				ii = new ImageIcon(this.getClass().getResource("Imagenak/irabazi2.png"));
				image[i] = ii.getImage();
			}
		}
		else if(id == 1)
		{
			for(int i=0; i<KotxeAnimatuak.k; i++)
			{
				ImageIcon ii;
				ii = new ImageIcon(this.getClass().getResource("Imagenak/irabazi1.png"));
				image[i] = ii.getImage();
			}
		}


	}
	
	public void setIrabazi2(int id)
	{
		if(id == 0)
		{
			for(int i=0; i<KotxeAnimatuak.k; i++)
			{
				ImageIcon ii;
				ii = new ImageIcon(this.getClass().getResource("Imagenak/irabazi1.png"));
				image[i] = ii.getImage();
			}
		}
		else if(id == 1)
		{
			for(int i=0; i<KotxeAnimatuak.k; i++)
			{
				ImageIcon ii;
				ii = new ImageIcon(this.getClass().getResource("Imagenak/irabazi2.png"));
				image[i] = ii.getImage();
			}
		}


	}

	public Panela(int kop){

		if(kop==1)
		{

			ImageIcon ii;
			//imagenak gehitu
			for(int i=0; i<KotxeAnimatuak.k+KotxeAnimatuak.kop; i++)
			{
				ii = new ImageIcon(this.getClass().getResource("Imagenak/zapoOna.png"));
				image[i] = ii.getImage();
			}

			//kotxeen posizioak hasieratu
			int pos=0;
			for(int i=0; i<KotxeAnimatuak.k+1; i++)
			{
				pos += 70;
				y[i]= pos;
				x[i]= -150;
			}
			//zapoaren posizioa hasieratu
			x[KotxeAnimatuak.k]=600;
			y[KotxeAnimatuak.k]=560;


			//atzeko irudia ezarri
			ImageIcon back1 = new ImageIcon(this.getClass().getResource("Imagenak/back3.png"));
			background = back1.getImage();

			timer = new Timer(15, this); // 15ms-ro actionPerformed metodoari deitzen dio
			timer.start();
		}
		else if(kop == 2)
		{
			//imagenak gehitu
			for(int i=0; i<KotxeAnimatuak.k+KotxeAnimatuak.kop; i++)
			{
				ImageIcon ii;
				ii = new ImageIcon(this.getClass().getResource("Imagenak/zapoOna.png"));
				image[i] = ii.getImage();
			}

			//kotxeen posizioak hasieratu
			int pos=0;
			for(int i=0; i<KotxeAnimatuak.k+1; i++)
			{
				pos += 70;
				y[i]= pos;
				x[i]= -150;
			}
			//zapoaren posizioa hasieratu
			x[KotxeAnimatuak.k]=600;
			y[KotxeAnimatuak.k]=560;

			x[KotxeAnimatuak.k+1]=400;
			y[KotxeAnimatuak.k+1]=560;


			//atzeko irudia ezarri
			ImageIcon back1 = new ImageIcon(this.getClass().getResource("Imagenak/back3.png"));
			background = back1.getImage();

			timer = new Timer(15, this); // 15ms-ro actionPerformed metodoari deitzen dio
			timer.start();
		}
	}

	public void paint(Graphics g){
		super.paint(g);	
		g.drawImage(background, 0, 0,1200,700, this);
		for(int i=0; i<KotxeAnimatuak.k+KotxeAnimatuak.kop; i++)
			g.drawImage(image[i], x[i] , y[i], this);
	}

	public void actionPerformed(ActionEvent e){ 
		repaint(); // panela bir-margotu (re-paint)
	}
}