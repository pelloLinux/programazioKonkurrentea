import java.io.*;
import Musika.Musika;
import Musika.Musika3;
import javazoom.jl.decoder.JavaLayerException;


public class KotxeAnimatuak  {

	static int k;
	static boolean jarrai;
	static Zapoa z;
	static Zapoa z1;
	static Zapoa z2;
	static int kop;
	static int jokalariKop;
	static Musika ms;
	static Musika3 ms3;

	public static void main (String args[]) {


		//atzeko musika erreproduzitu
		ms = new Musika();
		ms3 = new Musika3();
		ms.start();

		//zenbat jokalarik jokatu
		jarrai = false;
		GuiJava players = new GuiJava();
		try {
			
			jokatu();

		} catch (JavaLayerException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}


	}
	public static void jokatu() throws JavaLayerException, InterruptedException
	{

		k = 7;//errepideko kotxe kopurua
		int n = 700;
		int m = 1170;

		//zain egon jokalariak aukeratu arte
		while(!jarrai)Thread.sleep(100);

		

		if(jokalariKop==1)
		{
			kop = 1;
			Framea framea = new Framea(1,n,m);
			framea.setVisible(true);

			//errepidea sortu
			Errepidea e = new Errepidea(framea, n, m);		

			//zapoa sortu
			z = new Zapoa(0,600,8,e);

			Kotxea kotxea = null;
			//kotxeak sortu eta exekutatu
			while(jarrai){

				float random = (float) Math.random();
				if(random<0.3)
				{
					int abiadura = (int)(Math.random()*200+350);
					int karrila = (int)(Math.random()*7+1);
					kotxea = new Kotxea(karrila, abiadura, framea, e, "Imagenak/k1.png", "ezk");
					System.out.println(karrila+".kotxea = [ abiadura:"+abiadura+"]");
				}
				else if(random>=0.3 && random<0.4)
				{
					int abiadura = (int)(1000);
					int karrila = (int)(Math.random()*7+1);
					kotxea = new Kotxea(karrila, abiadura, framea, e, "Imagenak/kFormula.png", "esk");
					System.out.println(karrila+".kotxea = [ abiadura:"+abiadura+"]");
				}
				else if(random>=0.4 && random<0.6)
				{
					int abiadura = (int)(Math.random()*200+250);
					int karrila = (int)(Math.random()*7+1);
					kotxea = new Kotxea(karrila, abiadura, framea, e, "Imagenak/k3.png", "ezk");
					System.out.println(karrila+".kotxea = [ abiadura:"+abiadura+"]");
				}
				else if(random>=0.6 && random<0.9)
				{
					int abiadura = (int)(Math.random()*200+250);
					int karrila = (int)(Math.random()*7+1);
					kotxea = new Kotxea(karrila, abiadura, framea, e, "Imagenak/k4.png", "esk");
					System.out.println(karrila+".kotxea = [ abiadura:"+abiadura+"]");
				}
				else
				{
					int abiadura = (int)(Math.random()*200+250);
					int karrila = (int)(Math.random()*7+1);
					kotxea = new Kotxea(karrila, abiadura, framea, e, "Imagenak/k5.png", "ezk");
					System.out.println(karrila+".kotxea = [ abiadura:"+abiadura+"]");
				}


				kotxea.start();

				try {
					Thread.sleep((long) (Math.random()*500));
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			//bukatu da
			ms.stop();
			try {Thread.sleep(1000);} catch (InterruptedException e1) {e1.printStackTrace();}
			ms3.start();
			try {Thread.sleep(1000);} catch (InterruptedException e1) {e1.printStackTrace();}
			System.exit(0);

		}
		else if(jokalariKop==2)
		{
			kop=2;
			Framea framea = new Framea(2,n,m);
			framea.setVisible(true);

			//errepidea sortu
			Errepidea e = new Errepidea(framea, n, m);		

			//zapoa sortu
			z = new Zapoa(0,600,8,e);
			z1 = new Zapoa(1,400,8,e);

			Kotxea kotxea = null;
			//kotxeak sortu eta exekutatu
			while(jarrai){

				float random = (float) Math.random();
				if(random<0.3)
				{
					int abiadura = (int)(Math.random()*200+350);
					int karrila = (int)(Math.random()*7+1);
					kotxea = new Kotxea(karrila, abiadura, framea, e, "Imagenak/k1.png", "ezk");
					System.out.println(karrila+".kotxea = [ abiadura:"+abiadura+"]");
				}
				else if(random>=0.3 && random<0.4)
				{
					int abiadura = (int)(1000);
					int karrila = (int)(Math.random()*7+1);
					kotxea = new Kotxea(karrila, abiadura, framea, e, "Imagenak/kFormula.png", "esk");
					System.out.println(karrila+".kotxea = [ abiadura:"+abiadura+"]");
				}
				else if(random>=0.4 && random<0.6)
				{
					int abiadura = (int)(Math.random()*200+250);
					int karrila = (int)(Math.random()*7+1);
					kotxea = new Kotxea(karrila, abiadura, framea, e, "Imagenak/k3.png", "ezk");
					System.out.println(karrila+".kotxea = [ abiadura:"+abiadura+"]");
				}
				else if(random>=0.6 && random<0.9)
				{
					int abiadura = (int)(Math.random()*200+250);
					int karrila = (int)(Math.random()*7+1);
					kotxea = new Kotxea(karrila, abiadura, framea, e, "Imagenak/k4.png", "esk");
					System.out.println(karrila+".kotxea = [ abiadura:"+abiadura+"]");
				}
				else
				{
					int abiadura = (int)(Math.random()*200+250);
					int karrila = (int)(Math.random()*7+1);
					kotxea = new Kotxea(karrila, abiadura, framea, e, "Imagenak/k5.png", "ezk");
					System.out.println(karrila+".kotxea = [ abiadura:"+abiadura+"]");
				}


				kotxea.start();

				try {
					Thread.sleep((long) (Math.random()*500));
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			//bukatu da
			ms.stop();
			try {Thread.sleep(1000);} catch (InterruptedException e1) {e1.printStackTrace();}
			ms3.start();
			try {Thread.sleep(2000);} catch (InterruptedException e1) {e1.printStackTrace();}
			System.exit(0);
		}


	}
}




