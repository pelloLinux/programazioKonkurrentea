import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.print.attribute.standard.Media;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

class Kotxea extends Thread {
	private int kotxea;
	private float abiadura;
	private int denbora;
	private Framea framea;
	private Errepidea err;
	private String irudia;
	private String nora;

	public Kotxea(int kotx, float abiad, Framea f, Errepidea e,String i, String n){
		
		kotxea=kotx;
		abiadura=abiad;
		framea=f;
		err = e;
		irudia = i;
		nora = n;
	}

	public void run() {


		try {
			err.kotxeaJokoanSartu(this);

			//kotxea ezkerretik eskuinera
			if(this.nora.compareTo("esk")==0)
			{

				for (int x=-150;x<1200;x++) {

					err.kotxeaMugitu(kotxea, x, this);
					denbora=(int)(1000/abiadura);
					sleep(denbora);
				}
			}
			else if(this.nora.compareTo("ezk")==0)
			{
				for (int x=1200;x>-150;x--) {

					err.kotxeaMugitu(kotxea, x,this);
					denbora=(int)(1000/abiadura);
					sleep(denbora);
				}
			}
			err.kotxeaJokotikAtera(this);
			System.out.println(" Bukatuta ("+kotxea+")");

		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public int getKotxea()
	{
		return this.kotxea;
	}
	public String getIrudia()
	{
		return this.irudia;
	}
	public String getNora()
	{
		return this.nora;
	}
}