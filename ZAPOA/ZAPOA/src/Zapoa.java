
import Musika.Musika2;
import javazoom.jl.decoder.JavaLayerException;



public class Zapoa {


	public int izena;
	private Errepidea err;
	public int x;
	public int y;
	public int kop;
	public int yframe;
	public int xhasiera;
	public int yhasiera;
	Musika2 m;

	public Zapoa(int id,int x, int y, Errepidea e) throws JavaLayerException{

		izena = id; //zapoaren identifikadorea
		err = e;
		this.x = x;
		this.y = y;	
		xhasiera = x;
		yhasiera = y;
		yframe = 560;
		kop = 0;
	}

	public void mugituGora(){

		m = new Musika2();
		m.start();
		err.goraZapoa(this);

	}
	public void mugituBehera(){

		m = new Musika2();
		m.start();
		err.beheraZapoa(this);


	}
	public void mugituEzkerrera(){

		m = new Musika2();
		m.start();
		err.ezkZapoa(this);


	}
	public void mugituEskuinera(){

		m = new Musika2();
		m.start();
		err.eskZapoa(this);


	}
	

}




