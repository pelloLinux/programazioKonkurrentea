//librerias necesarias
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import javazoom.jl.decoder.JavaLayerException;

public class GuiJava implements ActionListener{//implementando el listener de eventos

	JButton bt1, bt2;//creando variables globales de los botones
	JFrame jf = new JFrame("ZAPOA");//creacion de ventana con el titulo

	public GuiJava(){//constructor de la clase


		jf.setLayout(new FlowLayout());//Configurar como se dispondra el espacio del jframe
		Dimension d = new Dimension();//objeto para obtener el ancho de la pantalla


		//Instanciando botones con texto
		bt1 = new JButton("Jokalari 1");
		bt2 = new JButton("Jokalari 2");

		//margenes para texto en boton
		bt1.setMargin(new Insets(20, 40, 20, 40));
		bt2.setMargin(new Insets(20, 40, 20, 40));

		//color de fondo del boton
		bt1.setBackground(Color.orange);

		//color de texto para el boton
		bt2.setBackground(Color.orange);


		//agregando los botones a la ventana
		jf.add(bt1); jf.add(bt2);

		//añadiendo el listener a los botones para manipular los eventos del click
		bt1.addActionListener(this);
		bt2.addActionListener(this);

		jf.getContentPane().setBackground(Color.BLACK);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//finaliza el programa cuando se da click en la X        
		jf.setResizable(false);//para configurar si se redimensiona la ventana
		jf.setLocation((int) (d.getWidth()+400), 270);//para ubicar inicialmente donde se muestra la ventana (x, y)
		jf.setSize(400, 130);//configurando tamaño de la ventana (ancho, alto)
		jf.setVisible(true);//configurando visualización de la venta
	}


	@Override
	public void actionPerformed(ActionEvent e) {//sobreescribimos el metodo del listener

		if(e.getActionCommand().equals("Jokalari 1")){//podemos comparar por el contenido del boton

			KotxeAnimatuak.jarrai = true;
			KotxeAnimatuak.jokalariKop = 1;
			jf.hide();
		}
		if(e.getSource()==bt2){//podemos comparar por el nombre del objeto del boton
			
			KotxeAnimatuak.jarrai = true;
			KotxeAnimatuak.jokalariKop = 2;
			jf.hide();
		}

	}
}