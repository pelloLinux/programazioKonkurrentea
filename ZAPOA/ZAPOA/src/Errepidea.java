
import Musika.Musika4;
import Musika.Musika5;


public class Errepidea {

	private int[][] errepidea;
	private Kotxea[] karrilak;
	private boolean[] karrilakOkupatuta;
	private int n;
	private int m;
	private Framea framea;
	private int A = 70;

	//eraikitzailea
	public Errepidea(Framea f, int n, int m){

		this.framea = f;
		errepidea = new int[m][9];// 7 karril eta 2 espaloi
		this.n = n;
		this.m = m;
		karrilak = new Kotxea[9];
		karrilakOkupatuta = new boolean[9];

	}


	//ZAPOA
	public synchronized void goraZapoa(Zapoa z)
	{
		int xZapoa = z.x;
		int yZapoa = z.y;

		if(yZapoa > 0)
		{
			//beste zapoa badagoen begiratu
			boolean libre = false;
			
			
			
			//matrizea eguneratu
			errepidea[xZapoa][yZapoa]=0;
			yZapoa--;
			System.out.println(xZapoa+":::"+yZapoa);

			//kotxea dagoen begiratu
			if(errepidea[xZapoa][yZapoa]==3)
			{
				System.out.println("txoke2:: "+xZapoa+"::"+yZapoa);
				framea.panela.setImageZapoa(z.izena);
				KotxeAnimatuak.jarrai = false;//gelditzeko jokoa
				Musika5 m = new Musika5();
				m.start();
				try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}

				if(KotxeAnimatuak.kop==1)
					framea.panela.setGameOver();
				else framea.panela.setIrabazi(z.izena);				


			}

			errepidea[xZapoa][yZapoa]=z.izena+1;

			framea.panela.setYZapoa(z.izena,true,A);

			//beste aldera iritsi da
			if(yZapoa==0)
			{
				Musika4 m = new Musika4();
				m.start();
				xZapoa = z.xhasiera;
				yZapoa = z.yhasiera;
				framea.panela.hasieraZapoa(z);
				System.out.println("IRITSITA");
				z.kop++;
				//5.era iritsi bada irabazi du
				if(z.kop==5)
				{
					framea.panela.setIrabazi2(z.izena);
					KotxeAnimatuak.jarrai = false;//gelditzeko jokoa
					
				}
			}

			z.x = xZapoa;
			z.y = yZapoa;
			notifyAll();
		}
	}

	public synchronized void beheraZapoa(Zapoa z)
	{
		int xZapoa = z.x;
		int yZapoa = z.y;

		if(yZapoa<8)
		{
			//beste zapoa badagoen begiratu
			boolean libre = false;
			
			//matrizea eguneratu
			errepidea[xZapoa][yZapoa]=0;
			yZapoa++;
			System.out.println(xZapoa+":::"+yZapoa);

			//kotxea dagoen begiratu
			if(errepidea[xZapoa][yZapoa]==3)
			{
				System.out.println("txoke2:: "+xZapoa+"::"+yZapoa);
				framea.panela.setImageZapoa(z.izena);
				KotxeAnimatuak.jarrai = false;//gelditzeko jokoa
				Musika5 m = new Musika5();
				m.start();
				try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}

				if(KotxeAnimatuak.kop==1)
					framea.panela.setGameOver();
				else framea.panela.setIrabazi(z.izena);

			}

			errepidea[xZapoa][yZapoa]=z.izena;

			framea.panela.setYZapoa(z.izena,false,A);
			z.x = xZapoa;
			z.y = yZapoa;
			notifyAll();

		}
	}

	public synchronized void eskZapoa(Zapoa z)
	{

		int xZapoa = z.x;
		int yZapoa = z.y;

		if(xZapoa<1080)
		{

			//matrizea eguneratu
			errepidea[xZapoa][yZapoa]=0;
			xZapoa+=A;
			System.out.println(xZapoa+":::"+yZapoa);

			//kotxea dagoen begiratu
			if(errepidea[xZapoa][yZapoa]==3)
			{
				System.out.println("txoke2:: "+xZapoa+"::"+yZapoa);
				framea.panela.setImageZapoa(z.izena);
				KotxeAnimatuak.jarrai = false;//gelditzeko jokoa
				Musika5 m = new Musika5();
				m.start();
				try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}

				if(KotxeAnimatuak.kop==1)
					framea.panela.setGameOver();
				else framea.panela.setIrabazi(z.izena);
			}

			errepidea[xZapoa][yZapoa]=z.izena+1;

			framea.panela.setXZapoa(z.izena,true,A);
			z.x = xZapoa;
			z.y = yZapoa;
			notifyAll();
		}
	}

	public synchronized void ezkZapoa(Zapoa z)
	{

		int xZapoa = z.x;
		int yZapoa = z.y;

		if(xZapoa > 30)
		{
			//matrizea eguneratu
			errepidea[xZapoa][yZapoa]=0;
			xZapoa-=A;
			System.out.println(xZapoa+":::"+yZapoa);

			//kotxea dagoen begiratu
			if(errepidea[xZapoa][yZapoa]==2)
			{
				System.out.println("txoke2:: "+xZapoa+"::"+yZapoa);
				framea.panela.setImageZapoa(z.izena);
				KotxeAnimatuak.jarrai = false;//gelditzeko jokoa
				Musika5 m = new Musika5();
				m.start();
				try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}

				if(KotxeAnimatuak.kop==1)
					framea.panela.setGameOver();
				else framea.panela.setIrabazi(z.izena);
			}

			//matrizea eguneratu
			errepidea[xZapoa][yZapoa]=z.izena+1;

			//posizioa aldatu
			framea.panela.setXZapoa(z.izena,false,A);
			z.x = xZapoa;
			z.y = yZapoa;
			notifyAll();
		}
	}

	//karrila libre badago kotxeak pantalla zehakatuko du
	public synchronized void kotxeaJokoanSartu(Kotxea k) throws InterruptedException{

		while(karrilakOkupatuta[k.getKotxea()]==true)wait();
		karrilakOkupatuta[k.getKotxea()]=true;
		karrilak[k.getKotxea()]=k;
		framea.panela.setImage(k);
		notifyAll();

	}

	//kotxeak pantalla zeharkatu du
	public synchronized void kotxeaJokotikAtera(Kotxea k){


		karrilak[k.getKotxea()]=null;
		karrilakOkupatuta[k.getKotxea()]=false;
		notifyAll();
	}


	///KOTXEAK
	public synchronized void kotxeaMugitu(int kotxea, int balioa, Kotxea k) 
	{
		//pantallaren barruan dagoen ala ez
		if(balioa > 0 && balioa < 1050){

			if(k.getNora().compareTo("esk")==0)
			{
				//zapoa jo badu
				if(errepidea[balioa+50][kotxea]==1 || errepidea[balioa+50][kotxea]==2){

					System.out.println("txokee:: "+balioa+"::"+kotxea);
					System.out.println(errepidea[balioa+50][kotxea]);
					framea.panela.setImageZapoa(errepidea[balioa+50][kotxea]-1);
					KotxeAnimatuak.jarrai = false;//gelditzeko jokoa
					Musika5 m = new Musika5();
					m.start();
					try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
					if(KotxeAnimatuak.kop==1)
						framea.panela.setGameOver();
					else framea.panela.setIrabazi(errepidea[balioa+50][kotxea]-1);
				}
				//matrizea eguneratu
				errepidea[balioa-1][kotxea]=0;
				for(int i=balioa; i<balioa+50; i++)
					errepidea[i][kotxea]=3;
			}
			else 
			{
				//zapoa jo badu
				if(errepidea[balioa][kotxea]==1 || errepidea[balioa][kotxea]==2){

					System.out.println("txokee:: "+balioa+"::"+kotxea);
					System.out.println(errepidea[balioa][kotxea]);
					framea.panela.setImageZapoa(errepidea[balioa][kotxea]-1);
					KotxeAnimatuak.jarrai = false;//gelditzeko jokoa
					Musika5 m = new Musika5();
					m.start();
					try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
					if(KotxeAnimatuak.kop==1)
						framea.panela.setGameOver();
					else framea.panela.setIrabazi(errepidea[balioa][kotxea]-1);
				}
				//matrizea eguneratu
				errepidea[balioa+50+1][kotxea]=0;
				for(int i=balioa; i<balioa+50; i++)
					errepidea[i][kotxea]=3;

			}
		}
		framea.panela.setXkotxea(kotxea,balioa);
		notifyAll();
	}


	private void mInprimatu()
	{
		for(int i=0; i<9; i++){
			for(int j=0; j<m; j++){
				System.out.print(errepidea[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("==============================================================");
	}

}
