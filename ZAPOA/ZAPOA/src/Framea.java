

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class Framea extends JFrame implements KeyListener {

	Panela panela;
	
	public Framea(int kop,int n, int m) {
		super();
		panela = new Panela(kop);
		initialize(n,m);
	}
	
	private void initialize(int n, int m) {
		this.setTitle("Kotxeak");
		this.add(panela);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(m,n);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setResizable(false);
		addKeyListener(this);
		
	}
	
	public void keyPressed(KeyEvent e) { 
		
		int keyCode = e.getKeyCode();
	    switch( keyCode ) { 
	        case KeyEvent.VK_UP:
	            // handle up 
	        	KotxeAnimatuak.z.mugituGora();
	            break;
	        case KeyEvent.VK_DOWN:
	            // handle down 
	        	KotxeAnimatuak.z.mugituBehera();
	            break;
	        case KeyEvent.VK_LEFT:
	            // handle left
	        	KotxeAnimatuak.z.mugituEzkerrera();
	            break;
	        case KeyEvent.VK_RIGHT :
	            // handle right
	        	KotxeAnimatuak.z.mugituEskuinera();
	            break;
	            
	        case KeyEvent.VK_W:
	            // handle up 
	        	KotxeAnimatuak.z1.mugituGora();
	            break;
	        case KeyEvent.VK_S:
	            // handle down 
	        	KotxeAnimatuak.z1.mugituBehera();
	            break;
	        case KeyEvent.VK_A:
	            // handle left
	        	KotxeAnimatuak.z1.mugituEzkerrera();
	            break;
	        case KeyEvent.VK_D:
	            // handle right
	        	KotxeAnimatuak.z1.mugituEskuinera();
	            break;
	     }
	}
	public void keyReleased(KeyEvent e) { }
	public void keyTyped(KeyEvent e) {
		System.out.println(e.getKeyChar());
	}

}

